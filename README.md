# README #

Dosyc prosty cms bloga, z nastepujacymi sekcjami (dodaj/usun/zmien)
* 1.Posty 
* 2.Kategorie
* 3.Strony
* 4.Ustawienia

Celem apki bylo tylko i wylacznie nauczenie sie angular2, wiec nie zajmowalem sie dokladnie zarzadzaniem bledami zwracanymi z api, ale wszystko dziala jak nalezy. Jest to tylko czesc frontendowa, czesc backendowa rowniez jest w moim repo o tej samej nazwie tylko z "backend" na koncu nazwy, opis backenda tam tez sie znajduje.

**Opinia ng2 i typescript:**
Podchodziłem do ts dosyć sceptycznie jako ze pierw probowalem na serwerze hostingowym dzialac i na quickstarcie bylo duzo errorow, ale jak juz odpalilem na localu to wszystko smigalo, uzywajac typescript mam wrazenie jakby kod byl lepiej ulozony , mniej chaotycznie niz w czystym JS. Angular2 zas wg mnie jest lepszy niz jedynka, wydaje sie prostszy jak juz sie zglebi jak to wszystko dziala, router jest co prawda inaczej zbudowany bo moze byc w kilku miejsach w zaleznosci gdzie jest parent i child ale wedlug mnie jest to lepsze rozwiazanie bo mozna ladnie podzielic aplikacje na czesci.

**Instalacja**
* 1.Sklonować lub sciagnac
* 2.npm install
* 3.npm run
* 4.Jesli wystapi blad "node_modules/ng2-ckeditor/CKEditor.d.ts(19,5): error TS7010: 'onValueChange', which lacks return-type annotation, implicitly has an 'any' return type." nalezy znalezc ten plik i dodac void, potem powinno hulac.
* 5.Zedytuj bootService.ts i ustaw swoja sciezke do api, w zmiennej _domain