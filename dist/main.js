"use strict";
var platform_browser_dynamic_1 = require('@angular/platform-browser-dynamic');
var app_component_1 = require('./components/app.component');
var bootService_1 = require('./service/bootService');
var localStorage_1 = require('./service/localStorage');
platform_browser_dynamic_1.bootstrap(app_component_1.AppComponent, [bootService_1.BootService, localStorage_1.LocalStorage]);
//# sourceMappingURL=main.js.map