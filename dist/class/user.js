"use strict";
var User = (function () {
    function User(login, password, api_token) {
        this.login = login;
        this.password = password;
        this.api_token = api_token;
    }
    return User;
}());
exports.User = User;
//# sourceMappingURL=user.js.map