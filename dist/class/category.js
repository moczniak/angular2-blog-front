"use strict";
var Category = (function () {
    /*
      categoryID: number;
    
      title:string;
      alias:string;
    */
    function Category(title, alias, categoryID) {
        this.title = title;
        this.alias = alias;
        this.categoryID = categoryID;
    }
    return Category;
}());
exports.Category = Category;
//# sourceMappingURL=category.js.map