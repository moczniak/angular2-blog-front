"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var router_deprecated_1 = require('@angular/router-deprecated');
var user_1 = require('./../../class/user');
var userService_1 = require('./../../service/userService');
var bootService_1 = require('./../../service/bootService');
var localStorage_1 = require('./../../service/localStorage');
var AdminLoginComponent = (function () {
    function AdminLoginComponent(_userService, _localStorage, _bootService, _router) {
        this._userService = _userService;
        this._localStorage = _localStorage;
        this._bootService = _bootService;
        this._router = _router;
        this.user = new user_1.User('', '');
    }
    AdminLoginComponent.prototype.ngOnInit = function () {
    };
    AdminLoginComponent.prototype.loginNow = function () {
        var _this = this;
        this._userService.loginAdmin(this.user.login, this.user.password)
            .subscribe(function (user) {
            _this._bootService.setToken(user.api_token);
            _this._localStorage.setKey('api_token', user.api_token);
            _this._localStorage.setExpiration();
            _this._router.navigate(['AdminDashboard']);
        }, function (error) { return _this.loginErr = error.message; });
    };
    AdminLoginComponent = __decorate([
        core_1.Component({
            selector: 'moczniak-admin-login',
            templateUrl: 'app/template/admin/adminLogin.component.html',
            providers: []
        }), 
        __metadata('design:paramtypes', [userService_1.UserService, localStorage_1.LocalStorage, bootService_1.BootService, router_deprecated_1.Router])
    ], AdminLoginComponent);
    return AdminLoginComponent;
}());
exports.AdminLoginComponent = AdminLoginComponent;
//# sourceMappingURL=adminLogin.component.js.map