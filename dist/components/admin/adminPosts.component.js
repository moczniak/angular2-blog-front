"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var router_deprecated_1 = require('@angular/router-deprecated');
var postService_1 = require('./../../service/postService');
var bootService_1 = require('./../../service/bootService');
var localStorage_1 = require('./../../service/localStorage');
var paginate_component_1 = require('./paginate.component');
var AdminPostsComponent = (function () {
    function AdminPostsComponent(_postService, _bootService, _localStorage, _router, _routeParams) {
        this._postService = _postService;
        this._bootService = _bootService;
        this._localStorage = _localStorage;
        this._router = _router;
        this._routeParams = _routeParams;
        this.limit = 10;
        this.page = 1;
        this.pageLink = 'AdminPostsPage';
    }
    AdminPostsComponent.prototype.ngOnInit = function () {
        var page = this._routeParams.get('page');
        if (page != null) {
            this.page = Number(page);
        }
        this.getPosts();
        this.pagination();
    };
    AdminPostsComponent.prototype.deletePost = function (postID) {
        var _this = this;
        this._postService.deletePost(postID)
            .subscribe(function (post) { _this.getPosts(); _this.pagination(); }, function (error) { return _this.handleError(error); });
    };
    AdminPostsComponent.prototype.editPost = function (post) {
        this._postService.setPost(post);
        this._router.navigate(['AdminPostEditor', { postID: post.postID }]);
    };
    AdminPostsComponent.prototype.getPosts = function () {
        var _this = this;
        this._postService.getPostPage(this.page, this.limit)
            .subscribe(function (post) { return _this.posts = post; }, function (error) { });
    };
    AdminPostsComponent.prototype.pagination = function () {
        var _this = this;
        this._postService.getPostCount()
            .subscribe(function (count) { return _this.paginate = count.count; }, function (error) { });
    };
    AdminPostsComponent.prototype.handleError = function (error) {
        if (error.status == 401) {
            this._bootService.setToken('');
            this._localStorage.clearAll();
            this._router.navigate(['AdminLogin']);
        }
    };
    AdminPostsComponent = __decorate([
        core_1.Component({
            selector: 'moczniak-admin-posts',
            templateUrl: 'app/template/admin/adminPosts.component.html',
            providers: [],
            directives: [router_deprecated_1.ROUTER_DIRECTIVES, paginate_component_1.PaginateComponent]
        }), 
        __metadata('design:paramtypes', [postService_1.PostService, bootService_1.BootService, localStorage_1.LocalStorage, router_deprecated_1.Router, router_deprecated_1.RouteParams])
    ], AdminPostsComponent);
    return AdminPostsComponent;
}());
exports.AdminPostsComponent = AdminPostsComponent;
//# sourceMappingURL=adminPosts.component.js.map