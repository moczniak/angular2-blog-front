"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var router_deprecated_1 = require('@angular/router-deprecated');
var pageService_1 = require('./../../service/pageService');
var bootService_1 = require('./../../service/bootService');
var localStorage_1 = require('./../../service/localStorage');
var paginate_component_1 = require('./paginate.component');
var AdminPagesComponent = (function () {
    function AdminPagesComponent(_pageService, _bootService, _localStorage, _router, _routeParams) {
        this._pageService = _pageService;
        this._bootService = _bootService;
        this._localStorage = _localStorage;
        this._router = _router;
        this._routeParams = _routeParams;
        this.limit = 10;
        this.page = 1;
        this.pageLink = 'AdminPagesPaginate';
    }
    AdminPagesComponent.prototype.ngOnInit = function () {
        var page = this._routeParams.get('page');
        if (page != null) {
            this.page = Number(page);
        }
        this.getPages();
        this.pagination();
    };
    AdminPagesComponent.prototype.deletePage = function (pageID) {
        var _this = this;
        this._pageService.deletePage(pageID)
            .subscribe(function (page) { _this.getPages(); _this.pagination(); }, function (error) { return _this.handleError(error); });
    };
    AdminPagesComponent.prototype.editPage = function (page) {
        this._pageService.setPage(page);
        this._router.navigate(['AdminPageEditor', { pageID: page.pageID }]);
    };
    AdminPagesComponent.prototype.getPages = function () {
        var _this = this;
        this._pageService.getPagePaginate(this.page, this.limit)
            .subscribe(function (page) { return _this.pages = page; }, function (error) { });
    };
    AdminPagesComponent.prototype.pagination = function () {
        var _this = this;
        this._pageService.getPagesCount()
            .subscribe(function (count) { return _this.paginate = count.count; }, function (error) { });
    };
    AdminPagesComponent.prototype.handleError = function (error) {
        if (error.status == 401) {
            this._bootService.setToken('');
            this._localStorage.clearAll();
            this._router.navigate(['AdminLogin']);
        }
    };
    AdminPagesComponent = __decorate([
        core_1.Component({
            selector: 'moczniak-admin-posts',
            templateUrl: 'app/template/admin/adminPages.component.html',
            providers: [],
            directives: [router_deprecated_1.ROUTER_DIRECTIVES, paginate_component_1.PaginateComponent]
        }), 
        __metadata('design:paramtypes', [pageService_1.PageService, bootService_1.BootService, localStorage_1.LocalStorage, router_deprecated_1.Router, router_deprecated_1.RouteParams])
    ], AdminPagesComponent);
    return AdminPagesComponent;
}());
exports.AdminPagesComponent = AdminPagesComponent;
//# sourceMappingURL=adminPages.component.js.map