"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var ng2_ckeditor_1 = require('ng2-ckeditor');
var router_deprecated_1 = require('@angular/router-deprecated');
var bootService_1 = require('./../../service/bootService');
var pageService_1 = require('./../../service/pageService');
var localStorage_1 = require('./../../service/localStorage');
var categoryService_1 = require('./../../service/categoryService');
var page_1 = require('./../../class/page');
var spinner_component_1 = require('./../front/spinner.component');
var AdminPageEditorComponent = (function () {
    function AdminPageEditorComponent(_pageService, _bootService, _localStorage, _router, _routeParams, _categoryService) {
        this._pageService = _pageService;
        this._bootService = _bootService;
        this._localStorage = _localStorage;
        this._router = _router;
        this._routeParams = _routeParams;
        this._categoryService = _categoryService;
        this.isHTTP = false;
        this.domain = this._bootService.getDomain();
        this.page = new page_1.Page();
        this.saved = false;
        this.savedErr = false;
        this.errorTxt = '';
    }
    AdminPageEditorComponent.prototype.ngOnInit = function () {
        var _this = this;
        var pageID = parseInt(this._routeParams.get('pageID'));
        if (pageID != 0) {
            var tmpPage = this._pageService.getPage();
            if (typeof tmpPage !== "undefined") {
                this.page = tmpPage;
            }
            else {
                this.isHTTP = true;
                this._pageService.getPageByPageID(pageID)
                    .subscribe(function (page) { return _this.page = page; }, function (error) { return _this.isHTTP = false; });
            }
        }
    };
    AdminPageEditorComponent.prototype.pageControl = function () {
        if (typeof this.page.pageID != "undefined") {
            this.updatePage();
        }
        else {
            this.addNewPage();
        }
    };
    AdminPageEditorComponent.prototype.updatePage = function () {
        var _this = this;
        this.isHTTP = true;
        this._pageService.updatePost(this.page)
            .subscribe(function (result) { return _this.handleSuccess(result); }, function (error) { return _this.handleError(error); });
    };
    AdminPageEditorComponent.prototype.addNewPage = function () {
        var _this = this;
        this.isHTTP = true;
        this._pageService.addNewPage(this.page)
            .subscribe(function (result) { return _this.handleSuccess(result); }, function (error) { return _this.handleError(error); });
    };
    AdminPageEditorComponent.prototype.handleSuccess = function (success) {
        var _this = this;
        this.isHTTP = false;
        this.page = success;
        this.saved = true;
        setTimeout(function () {
            _this.saved = false;
        }, 3000);
    };
    AdminPageEditorComponent.prototype.handleError = function (error) {
        var _this = this;
        this.isHTTP = false;
        if (error.status == 401) {
            this._bootService.setToken('');
            this._localStorage.clearAll();
            this._router.navigate(['AdminLogin']);
        }
        else {
            this.savedErr = true;
            this.errorTxt = 'Podany alias już istnieje';
            setTimeout(function () {
                _this.savedErr = false;
            }, 3000);
        }
    };
    AdminPageEditorComponent = __decorate([
        core_1.Component({
            selector: 'moczniak-admin-newPost',
            templateUrl: 'app/template/admin/adminPageEditor.component.html',
            directives: [ng2_ckeditor_1.CKEditor, spinner_component_1.SpinnerComponent],
            providers: [categoryService_1.CategoryService]
        }), 
        __metadata('design:paramtypes', [pageService_1.PageService, bootService_1.BootService, localStorage_1.LocalStorage, router_deprecated_1.Router, router_deprecated_1.RouteParams, categoryService_1.CategoryService])
    ], AdminPageEditorComponent);
    return AdminPageEditorComponent;
}());
exports.AdminPageEditorComponent = AdminPageEditorComponent;
//# sourceMappingURL=adminPageEditor.component.js.map