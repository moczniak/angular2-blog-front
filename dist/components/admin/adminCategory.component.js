"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var router_deprecated_1 = require('@angular/router-deprecated');
var category_1 = require('./../../class/category');
var categoryService_1 = require('./../../service/categoryService');
var bootService_1 = require('./../../service/bootService');
var localStorage_1 = require('./../../service/localStorage');
var AdminCategoryComponent = (function () {
    function AdminCategoryComponent(_categoryService, _router, _bootService, _localStorage) {
        this._categoryService = _categoryService;
        this._router = _router;
        this._bootService = _bootService;
        this._localStorage = _localStorage;
        this.modelCategory = new category_1.Category('', '');
        this.modelCategoryEdit = new category_1.Category('', '');
        this.editCategoryStatus = false;
        this.saved = false;
        this.errorTxt = '';
    }
    AdminCategoryComponent.prototype.ngOnInit = function () {
        this.getCategories();
    };
    AdminCategoryComponent.prototype.saveCategoryEdit = function () {
        var _this = this;
        this._categoryService.updateCategory(this.modelCategoryEdit)
            .subscribe(function (category) {
            for (var i = 0; i < _this.category.length; ++i) {
                if (_this.category[i].categoryID == _this.modelCategoryEdit.categoryID) {
                    _this.category[i] = _this.modelCategoryEdit;
                    _this.editCategoryStatus = false;
                    break;
                }
            }
        }, function (error) { return _this.handleError(error); });
    };
    AdminCategoryComponent.prototype.editCategory = function (category) {
        this.modelCategoryEdit = Object.assign({}, category);
        this.editCategoryStatus = true;
    };
    AdminCategoryComponent.prototype.saveCategory = function () {
        var _this = this;
        console.log(this.modelCategory);
        this._categoryService.addNewCategory(this.modelCategory)
            .subscribe(function (category) {
            _this.category.push(category);
            _this.modelCategory.title = '';
            _this.modelCategory.alias = '';
            _this.modelCategory.categoryID = 0;
        }, function (error) { return _this.handleError(error); });
    };
    AdminCategoryComponent.prototype.getCategories = function () {
        var _this = this;
        this._categoryService.getCategories()
            .subscribe(function (category) { return _this.category = category; }, function (error) { });
    };
    AdminCategoryComponent.prototype.deleteCategory = function (categoryID) {
        var _this = this;
        this._categoryService.deleteCategory(categoryID)
            .subscribe(function (category) {
            for (var i = 0; i < _this.category.length; ++i) {
                if (_this.category[i].categoryID == categoryID) {
                    _this.category.splice(i, 1);
                    break;
                }
            }
        }, function (error) { return _this.handleError(error); });
    };
    AdminCategoryComponent.prototype.handleError = function (error) {
        var _this = this;
        if (error.status == 401) {
            this._bootService.setToken('');
            this._localStorage.clearAll();
            this._router.navigate(['AdminLogin']);
        }
        else {
            this.saved = true;
            this.errorTxt = 'Podany Tytuł lub alias już istnieje';
            setTimeout(function () {
                _this.saved = false;
            }, 3000);
        }
    };
    AdminCategoryComponent = __decorate([
        core_1.Component({
            selector: 'moczniak-admin-category',
            templateUrl: 'app/template/admin/adminCategory.component.html',
            providers: [categoryService_1.CategoryService]
        }), 
        __metadata('design:paramtypes', [categoryService_1.CategoryService, router_deprecated_1.Router, bootService_1.BootService, localStorage_1.LocalStorage])
    ], AdminCategoryComponent);
    return AdminCategoryComponent;
}());
exports.AdminCategoryComponent = AdminCategoryComponent;
//# sourceMappingURL=adminCategory.component.js.map