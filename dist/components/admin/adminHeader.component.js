"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var router_deprecated_1 = require('@angular/router-deprecated');
var http_1 = require('@angular/http');
var bootService_1 = require('./../../service/bootService');
var userService_1 = require('./../../service/userService');
var localStorage_1 = require('./../../service/localStorage');
var postService_1 = require('./../../service/postService');
var pageService_1 = require('./../../service/pageService');
var adminDashboard_component_1 = require('./adminDashboard.component');
var adminLogin_component_1 = require('./adminLogin.component');
var adminPosts_component_1 = require('./adminPosts.component');
var adminPostEditor_component_1 = require('./adminPostEditor.component');
var adminCategory_component_1 = require('./adminCategory.component');
var adminSetting_component_1 = require('./adminSetting.component');
var adminPages_component_1 = require('./adminPages.component');
var adminPageEditor_component_1 = require('./adminPageEditor.component');
var AdminHeaderComponent = (function () {
    function AdminHeaderComponent(_userService, _localStorage, _bootService, _router) {
        var _this = this;
        this._userService = _userService;
        this._localStorage = _localStorage;
        this._bootService = _bootService;
        this._router = _router;
        this.loged = false;
        this.checkLoged();
        this._bootService.isToken$.subscribe(function (value) { return _this.loged = value; });
    }
    AdminHeaderComponent.prototype.tryAuth = function () {
        var _this = this;
        this._userService.tryAuth()
            .subscribe(function (auth) { return _this.loged = true; }, function (errorStatus) {
            if (errorStatus == 401) {
                _this._bootService.setToken('');
                _this._localStorage.clearAll();
                _this.goToLogin();
            }
        });
    };
    AdminHeaderComponent.prototype.goToLogin = function () {
        this._router.navigate(['AdminLogin']);
    };
    AdminHeaderComponent.prototype.checkLoged = function () {
        if (this._bootService.getToken() == '') {
            if (typeof this._localStorage.getKey('api_token') !== 'undefined') {
                this._bootService.setToken(this._localStorage.getKey('api_token'));
                this.tryAuth();
            }
            else
                this.goToLogin();
        }
        else
            this.tryAuth();
    };
    AdminHeaderComponent = __decorate([
        core_1.Component({
            selector: 'moczniak-admin',
            templateUrl: 'app/template/admin/adminHeader.component.html',
            styleUrls: ['app/css/bootstrap.min.css', 'app/css/stylesAdmin.css'],
            encapsulation: core_1.ViewEncapsulation.None,
            directives: [router_deprecated_1.ROUTER_DIRECTIVES],
            providers: [http_1.HTTP_PROVIDERS, userService_1.UserService, localStorage_1.LocalStorage, postService_1.PostService, pageService_1.PageService]
        }),
        router_deprecated_1.RouteConfig([
            {
                path: '/dashboard',
                name: 'AdminDashboard',
                component: adminDashboard_component_1.AdminDashboardComponent,
                useAsDefault: true
            }, {
                path: '/settings',
                name: 'AdminSetting',
                component: adminSetting_component_1.AdminSettingComponent
            }, {
                path: '/pages',
                name: 'AdminPages',
                component: adminPages_component_1.AdminPagesComponent
            }, {
                path: '/pages/page/:page',
                name: 'AdminPagesPaginate',
                component: adminPages_component_1.AdminPagesComponent
            }, {
                path: '/category',
                name: 'AdminCategory',
                component: adminCategory_component_1.AdminCategoryComponent
            }, {
                path: '/login',
                name: 'AdminLogin',
                component: adminLogin_component_1.AdminLoginComponent
            }, {
                path: '/posts',
                name: 'AdminPosts',
                component: adminPosts_component_1.AdminPostsComponent
            }, {
                path: '/posts/page/:page',
                name: 'AdminPostsPage',
                component: adminPosts_component_1.AdminPostsComponent
            }, {
                path: '/postEditor/:postID',
                name: 'AdminPostEditor',
                component: adminPostEditor_component_1.AdminPostEditorComponent
            }, {
                path: '/pageEditor/:pageID',
                name: 'AdminPageEditor',
                component: adminPageEditor_component_1.AdminPageEditorComponent
            }
        ]), 
        __metadata('design:paramtypes', [userService_1.UserService, localStorage_1.LocalStorage, bootService_1.BootService, router_deprecated_1.Router])
    ], AdminHeaderComponent);
    return AdminHeaderComponent;
}());
exports.AdminHeaderComponent = AdminHeaderComponent;
//# sourceMappingURL=adminHeader.component.js.map