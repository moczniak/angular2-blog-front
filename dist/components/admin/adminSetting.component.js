"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var settingService_1 = require('./../../service/settingService');
var userService_1 = require('./../../service/userService');
var AdminSettingComponent = (function () {
    function AdminSettingComponent(_settingService, _userService) {
        this._settingService = _settingService;
        this._userService = _userService;
        this.user = { passwordcr: '', password: '', passwordcf: '' };
        this.loginErr = '';
        this.loginSucc = '';
    }
    AdminSettingComponent.prototype.ngOnInit = function () {
        this.getSettings();
    };
    AdminSettingComponent.prototype.getSettings = function () {
        var _this = this;
        this._settingService.getSetting()
            .subscribe(function (settings) { return _this.setting = settings; }, function (error) { });
    };
    AdminSettingComponent.prototype.saveSettings = function () {
        this._settingService.saveSettings(this.setting)
            .subscribe(function (setting) { return console.log(setting); }, function (error) { return console.log(error); });
    };
    AdminSettingComponent.prototype.saveLoginData = function () {
        var _this = this;
        if (this.user.password === this.user.passwordcf) {
            this._userService.changePassword(this.user)
                .subscribe(function (user) {
                _this.loginSucc = user.message;
                _this.hidePop();
            }, function (error) {
                _this.loginErr = error.message;
                _this.hidePop();
            });
        }
        else {
            this.loginErr = 'Hasla nie zgadzają się ze sobą';
            this.hidePop();
        }
    };
    AdminSettingComponent.prototype.hidePop = function () {
        var _this = this;
        setTimeout(function () {
            _this.loginErr = '';
            _this.loginSucc = '';
        }, 3000);
    };
    AdminSettingComponent = __decorate([
        core_1.Component({
            selector: 'moczniak-admin-setting',
            templateUrl: 'app/template/admin/adminSetting.component.html',
            providers: [settingService_1.SettingService, userService_1.UserService]
        }), 
        __metadata('design:paramtypes', [settingService_1.SettingService, userService_1.UserService])
    ], AdminSettingComponent);
    return AdminSettingComponent;
}());
exports.AdminSettingComponent = AdminSettingComponent;
//# sourceMappingURL=adminSetting.component.js.map