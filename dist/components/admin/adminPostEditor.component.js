"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var ng2_ckeditor_1 = require('ng2-ckeditor');
var router_deprecated_1 = require('@angular/router-deprecated');
var bootService_1 = require('./../../service/bootService');
var postService_1 = require('./../../service/postService');
var localStorage_1 = require('./../../service/localStorage');
var categoryService_1 = require('./../../service/categoryService');
var post_1 = require('./../../class/post');
var spinner_component_1 = require('./../front/spinner.component');
var AdminPostEditorComponent = (function () {
    function AdminPostEditorComponent(_postService, _bootService, _localStorage, _router, _routeParams, _categoryService) {
        this._postService = _postService;
        this._bootService = _bootService;
        this._localStorage = _localStorage;
        this._router = _router;
        this._routeParams = _routeParams;
        this._categoryService = _categoryService;
        this.isHTTP = false;
        this.domain = this._bootService.getDomain();
        this.post = new post_1.Post();
        this.modelCategory = 0;
        this.saved = false;
        this.savedErr = false;
        this.errorTxt = '';
    }
    AdminPostEditorComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.loadCategory();
        var postID = parseInt(this._routeParams.get('postID'));
        if (postID != 0) {
            var tmpPost = this._postService.getPost();
            if (typeof tmpPost !== "undefined") {
                this.post = tmpPost;
                if (typeof this.post.category != 'undefined')
                    this.modelCategory = this.post.category.categoryID;
            }
            else {
                this.isHTTP = true;
                this._postService.getPostByPostID(postID)
                    .subscribe(function (post) {
                    _this.post = post;
                    if (_this.post.promoImage != "")
                        _this.post.promoImage = _this.domain + _this.post.promoImage;
                    _this.isHTTP = false;
                    if (typeof _this.post.category != 'undefined')
                        _this.modelCategory = _this.post.category.categoryID;
                }, function (error) { _this.isHTTP = false; });
            }
        }
    };
    AdminPostEditorComponent.prototype.loadCategory = function () {
        var _this = this;
        this._categoryService.getCategories()
            .subscribe(function (categories) {
            _this.category = categories;
            _this.category.unshift({ categoryID: 0, alias: '', title: "Brak" });
        }, function (error) { });
    };
    AdminPostEditorComponent.prototype.categoryChange = function ($event) {
        this.post.categoryID = $event;
    };
    AdminPostEditorComponent.prototype.postControl = function () {
        if (typeof this.post.postID != "undefined") {
            this.updatePost();
        }
        else {
            this.addNewPost();
        }
    };
    AdminPostEditorComponent.prototype.updatePost = function () {
        var _this = this;
        this.isHTTP = true;
        this._postService.updatePost(this.post)
            .subscribe(function (result) { return _this.handleSuccess(result); }, function (error) { return _this.handleError(error); }, function () { _this.isHTTP = false; console.log("enddd"); });
    };
    AdminPostEditorComponent.prototype.addNewPost = function () {
        var _this = this;
        this.isHTTP = true;
        this._postService.addNewPost(this.post)
            .subscribe(function (result) { return _this.handleSuccess(result); }, function (error) { return _this.handleError(error); });
    };
    AdminPostEditorComponent.prototype.handleSuccess = function (success) {
        var _this = this;
        this.isHTTP = false;
        this.post = success;
        if (this.post.promoImage != "") {
            this.post.promoImage = this.domain + this.post.promoImage;
        }
        this.saved = true;
        setTimeout(function () {
            _this.saved = false;
        }, 3000);
    };
    AdminPostEditorComponent.prototype.handleError = function (error) {
        var _this = this;
        this.isHTTP = false;
        if (error.status == 401) {
            this._bootService.setToken('');
            this._localStorage.clearAll();
            this._router.navigate(['AdminLogin']);
        }
        else {
            this.savedErr = true;
            this.errorTxt = 'Podany alias już istnieje';
            setTimeout(function () {
                _this.savedErr = false;
            }, 3000);
        }
    };
    AdminPostEditorComponent.prototype.filechanger = function ($event) {
        this.readThis($event.target);
    };
    AdminPostEditorComponent.prototype.readThis = function (inputValue) {
        var _this = this;
        var file = inputValue.files[0];
        var myReader = new FileReader();
        myReader.onloadend = function (e) {
            _this.post.promoImage = myReader.result;
        };
        try {
            myReader.readAsDataURL(file);
        }
        catch (err) {
            this.post.promoImage = '';
        }
    };
    AdminPostEditorComponent = __decorate([
        core_1.Component({
            selector: 'moczniak-admin-newPost',
            templateUrl: 'app/template/admin/adminPostEditor.component.html',
            directives: [ng2_ckeditor_1.CKEditor, spinner_component_1.SpinnerComponent],
            providers: [categoryService_1.CategoryService]
        }), 
        __metadata('design:paramtypes', [postService_1.PostService, bootService_1.BootService, localStorage_1.LocalStorage, router_deprecated_1.Router, router_deprecated_1.RouteParams, categoryService_1.CategoryService])
    ], AdminPostEditorComponent);
    return AdminPostEditorComponent;
}());
exports.AdminPostEditorComponent = AdminPostEditorComponent;
//# sourceMappingURL=adminPostEditor.component.js.map