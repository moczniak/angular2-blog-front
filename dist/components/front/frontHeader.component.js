"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var router_deprecated_1 = require('@angular/router-deprecated');
var http_1 = require('@angular/http');
var mainPage_component_1 = require('./mainPage.component');
var postPage_component_1 = require('./postPage.component');
var postService_1 = require('./../../service/postService');
var categoryService_1 = require('./../../service/categoryService');
var settingService_1 = require('./../../service/settingService');
var seoService_1 = require('./../../service/seoService');
var bootService_1 = require('./../../service/bootService');
var sidebar_component_1 = require('./sidebar.component');
var footer_component_1 = require('./footer.component');
var FrontHeaderComponent = (function () {
    function FrontHeaderComponent(_router, _categoryService, _settingService, _seoService, _bootService) {
        this._router = _router;
        this._categoryService = _categoryService;
        this._settingService = _settingService;
        this._seoService = _seoService;
        this._bootService = _bootService;
        this.title = '';
        this.categoryActive = 0;
        this._router.subscribe(function (val) { return console.log(val); });
    }
    FrontHeaderComponent.prototype.ngOnInit = function () {
        this.loadCategories();
        this.loadSettings();
        $("body").tooltip({
            selector: '[data-toggle="tooltip"]'
        });
        console.log("load!");
    };
    FrontHeaderComponent.prototype.loadCategories = function () {
        var _this = this;
        this._categoryService.getCategories()
            .subscribe(function (categories) { return _this.categories = categories; }, function (error) { return _this.errMessage = error; });
    };
    FrontHeaderComponent.prototype.loadSettings = function () {
        var _this = this;
        this._settingService.getSetting()
            .subscribe(function (setting) {
            _this._bootService.setSiteTitle(setting.siteTitle);
            _this._seoService.setTitle(setting.title);
            _this._seoService.setKeywords(setting.keywords);
            _this._seoService.setDescription(setting.description);
            _this._seoService.setDefaultDesc(setting.description);
            _this._seoService.setDefaultTitle(setting.title);
            _this._seoService.setDefaultKeywords(setting.keywords);
            _this.title = setting.siteTitle;
        }, function (error) { return console.log(error); });
    };
    FrontHeaderComponent.prototype.isActiveRoute = function (routeName) {
        return routeName == this._router.currentInstruction.component.routeName;
    };
    FrontHeaderComponent = __decorate([
        core_1.Component({
            selector: 'moczniak-front',
            templateUrl: 'app/template/front/frontHeader.component.html',
            styleUrls: ['app/css/bootstrapCyborg.min.css', 'app/css/styles.css'],
            encapsulation: core_1.ViewEncapsulation.None,
            directives: [router_deprecated_1.ROUTER_DIRECTIVES, sidebar_component_1.SidebarComponent, footer_component_1.FooterComponent],
            providers: [postService_1.PostService, categoryService_1.CategoryService, http_1.HTTP_PROVIDERS, seoService_1.SeoService, settingService_1.SettingService]
        }),
        router_deprecated_1.RouteConfig([
            {
                path: '/',
                name: 'Start',
                component: mainPage_component_1.MainPageComponent,
                useAsDefault: true
            }, {
                path: '/page/:page',
                name: 'StartPages',
                component: mainPage_component_1.MainPageComponent
            }, {
                path: '/post/:postID/:alias',
                name: 'Post',
                component: postPage_component_1.PostPageComponent
            }, {
                path: '/category/:categoryID/:alias',
                name: 'Category',
                component: mainPage_component_1.MainPageComponent,
            }, {
                path: '/category/:categoryID/:alias/page/:page',
                name: 'CategoryPages',
                component: mainPage_component_1.MainPageComponent
            }
        ]), 
        __metadata('design:paramtypes', [router_deprecated_1.Router, categoryService_1.CategoryService, settingService_1.SettingService, seoService_1.SeoService, bootService_1.BootService])
    ], FrontHeaderComponent);
    return FrontHeaderComponent;
}());
exports.FrontHeaderComponent = FrontHeaderComponent;
//# sourceMappingURL=frontHeader.component.js.map