"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var http_1 = require('@angular/http');
var router_deprecated_1 = require('@angular/router-deprecated');
var paginate_component_1 = require('./paginate.component');
var postService_1 = require('./../../service/postService');
var bootService_1 = require('./../../service/bootService');
var seoService_1 = require('./../../service/seoService');
var spinner_component_1 = require('./spinner.component');
var MainPageComponent = (function () {
    function MainPageComponent(_bootService, _postService, _router, _routeParams, _seoService) {
        this._bootService = _bootService;
        this._postService = _postService;
        this._router = _router;
        this._routeParams = _routeParams;
        this._seoService = _seoService;
        this.domain = this._bootService.getDomain();
        this.limit = 2;
        this.page = 1;
        this.pageLink = 'StartPages';
        this.isHTTP = false;
        this.otherArgsLink = {};
    }
    MainPageComponent.prototype.ngOnInit = function () {
        var categoryID = this._routeParams.get('categoryID');
        if (categoryID != null) {
            this.mainPageFromCategory();
        }
        else {
            this.mainPage();
        }
        this._seoService.setTitle();
        this._seoService.setDescription();
        this._seoService.setKeywords();
    };
    MainPageComponent.prototype.mainPageFromCategory = function () {
        var categoryID = Number(this._routeParams.get('categoryID'));
        var alias = this._routeParams.get('alias');
        var page = this._routeParams.get('page');
        this.pageLink = 'CategoryPages';
        this.otherArgsLink = { categoryID: categoryID, alias: alias };
        if (page != null) {
            this.page = Number(page);
        }
        this.getPostsFromCategory(categoryID);
        this.paginationFromCategory(categoryID);
    };
    MainPageComponent.prototype.mainPage = function () {
        var page = this._routeParams.get('page');
        var tempPaginate = this._bootService.getTempKey('paginate');
        if (page != null) {
            this.page = Number(page);
        }
        this.getPosts();
        if (typeof tempPaginate == 'undefined') {
            this.pagination();
        }
        else {
            this.paginate = tempPaginate;
        }
    };
    MainPageComponent.prototype.routePost = function (post) {
        this._postService.setPost(post);
        this._router.navigate(['Post', { postID: post.postID, alias: post.alias }]);
    };
    MainPageComponent.prototype.getPostsFromCategory = function (categoryID) {
        var _this = this;
        this.isHTTP = true;
        this._postService.getPostPageFromCategory(categoryID, this.page, this.limit)
            .subscribe(function (post) { _this.posts = post, _this.isHTTP = false; }, function (error) { _this.isHTTP = false; });
    };
    MainPageComponent.prototype.getPosts = function () {
        var _this = this;
        this.isHTTP = true;
        this._postService.getPostPage(this.page, this.limit)
            .subscribe(function (post) { _this.posts = post, _this.isHTTP = false; }, function (error) { _this.isHTTP = false; });
    };
    MainPageComponent.prototype.paginationFromCategory = function (categoryID) {
        var _this = this;
        this.isHTTP = true;
        this._postService.getPostFromCategoryCount(categoryID)
            .subscribe(function (count) { _this.paginate = count.count; _this.isHTTP = false; }, function (error) { _this.isHTTP = false; });
    };
    MainPageComponent.prototype.pagination = function () {
        var _this = this;
        this._postService.getPostCount()
            .subscribe(function (count) { _this.paginate = count.count; _this._bootService.setTempKey('paginate', count.count); }, function (error) { });
    };
    MainPageComponent = __decorate([
        core_1.Component({
            selector: 'moczniak-posts',
            templateUrl: 'app/template/front/mainPage.component.html',
            directives: [paginate_component_1.PaginateComponent, spinner_component_1.SpinnerComponent],
            providers: [http_1.HTTP_PROVIDERS]
        }), 
        __metadata('design:paramtypes', [bootService_1.BootService, postService_1.PostService, router_deprecated_1.Router, router_deprecated_1.RouteParams, seoService_1.SeoService])
    ], MainPageComponent);
    return MainPageComponent;
}());
exports.MainPageComponent = MainPageComponent;
//# sourceMappingURL=mainPage.component.js.map