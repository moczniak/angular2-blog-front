"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var router_deprecated_1 = require('@angular/router-deprecated');
var footerService_1 = require('./../../service/footerService');
var pageService_1 = require('./../../service/pageService');
var bootService_1 = require('./../../service/bootService');
var postService_1 = require('./../../service/postService');
var FooterComponent = (function () {
    function FooterComponent(_router, _footerService, _pageService, _postService, _bootService) {
        this._router = _router;
        this._footerService = _footerService;
        this._pageService = _pageService;
        this._postService = _postService;
        this._bootService = _bootService;
        this.content = [];
    }
    FooterComponent.prototype.ngOnInit = function () {
        this.getFooters();
    };
    FooterComponent.prototype.getFooters = function () {
        var _this = this;
        this._footerService.getFooters()
            .subscribe(function (footer) {
            for (var i = 0; i < footer.length; ++i) {
                try {
                    var data = JSON.parse(footer[i].content);
                    if (data[0] == 'page') {
                        _this.getPage(data[1].pageID, footer[i].position);
                    }
                    else if (data[0] == 'popular') {
                        _this.getPopular(footer[i].position);
                    }
                }
                catch (err) {
                    _this.content.push({ content: footer[i].content, position: footer[i].position });
                }
            }
        }, function (error) { console.log(error); });
    };
    FooterComponent.prototype.getPopular = function (position) {
        var _this = this;
        this._postService.getPopular()
            .subscribe(function (popular) {
            _this.popular = popular;
            _this.content.push({ content: 'popular', position: position });
        }, function (error) { });
    };
    FooterComponent.prototype.routePost = function (post) {
        this._postService.setPost(post);
        this._router.navigate(['Post', { postID: post.postID, alias: post.alias }]);
    };
    FooterComponent.prototype.getPage = function (pageID, position) {
        var _this = this;
        this._pageService.getPageByPageID(pageID)
            .subscribe(function (page) {
            _this.content.push({ content: page.content, position: position });
        }, function (error) { console.log(error); });
    };
    FooterComponent = __decorate([
        core_1.Component({
            selector: 'moczniak-footer',
            templateUrl: 'app/template/front/footer.component.html',
            providers: [footerService_1.FooterService, pageService_1.PageService],
            directives: [router_deprecated_1.ROUTER_DIRECTIVES]
        }), 
        __metadata('design:paramtypes', [router_deprecated_1.Router, footerService_1.FooterService, pageService_1.PageService, postService_1.PostService, bootService_1.BootService])
    ], FooterComponent);
    return FooterComponent;
}());
exports.FooterComponent = FooterComponent;
//# sourceMappingURL=footer.component.js.map