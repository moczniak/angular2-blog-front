"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var router_deprecated_1 = require('@angular/router-deprecated');
var bootService_1 = require('./../../service/bootService');
var postService_1 = require('./../../service/postService');
var PopularPostSidebarComponent = (function () {
    function PopularPostSidebarComponent(_postService, _bootService) {
        this._postService = _postService;
        this._bootService = _bootService;
        this.domain = this._bootService.getDomain();
    }
    PopularPostSidebarComponent.prototype.ngOnInit = function () {
        this.GetPopularPost();
    };
    PopularPostSidebarComponent.prototype.GetPopularPost = function () {
        var _this = this;
        this._postService.getPopular()
            .subscribe(function (popular) { return _this.popular = popular; }, function (error) { });
    };
    PopularPostSidebarComponent = __decorate([
        core_1.Component({
            selector: 'moczniak-sidebar-popularPost',
            templateUrl: 'app/template/front/popularPostSidebar.component.html',
            directives: [router_deprecated_1.ROUTER_DIRECTIVES]
        }), 
        __metadata('design:paramtypes', [postService_1.PostService, bootService_1.BootService])
    ], PopularPostSidebarComponent);
    return PopularPostSidebarComponent;
}());
exports.PopularPostSidebarComponent = PopularPostSidebarComponent;
//# sourceMappingURL=popularPostSidebar.component.js.map