"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var router_deprecated_1 = require('@angular/router-deprecated');
var postService_1 = require('./../../service/postService');
var bootService_1 = require('./../../service/bootService');
var seoService_1 = require('./../../service/seoService');
var spinner_component_1 = require('./spinner.component');
var PostPageComponent = (function () {
    function PostPageComponent(_postService, _routeParams, _bootService, _seoService) {
        this._postService = _postService;
        this._routeParams = _routeParams;
        this._bootService = _bootService;
        this._seoService = _seoService;
        this.domain = this._bootService.getDomain();
        this.isHTTP = false;
    }
    PostPageComponent.prototype.ngOnInit = function () {
        this.getPost();
    };
    PostPageComponent.prototype.getPost = function () {
        var postID = this._routeParams.get('postID');
        var post = this._postService.getPost();
        if (typeof post === 'undefined') {
            this.loadPost(parseInt(postID));
        }
        else {
            if (post.postID == parseInt(postID)) {
                this.post = post;
                this.setSeo();
                this.setComment(post.postID);
            }
            else {
                this.loadPost(parseInt(postID));
            }
        }
    };
    PostPageComponent.prototype.setComment = function (postID) {
        DISQUS.reset({
            reload: true,
            config: function () {
                this.page.identifier = postID;
            }
        });
    };
    PostPageComponent.prototype.loadPost = function (postID) {
        var _this = this;
        this.isHTTP = true;
        this._postService.getPostByPostID(postID)
            .subscribe(function (post) {
            _this.post = post;
            _this.setSeo();
            _this.setComment(post.postID);
        }, function (error) { return _this.errorMessage = error; }, function () { return _this.isHTTP = false; });
    };
    PostPageComponent.prototype.setSeo = function () {
        this._seoService.setTitle(this.post.seoTitle);
        this._seoService.setDescription(this.post.seoDesc);
        this._seoService.setKeywords(this.post.seoKeywords);
    };
    PostPageComponent = __decorate([
        core_1.Component({
            selector: 'moczniak-post',
            templateUrl: 'app/template/front/postPage.component.html',
            directives: [spinner_component_1.SpinnerComponent]
        }), 
        __metadata('design:paramtypes', [postService_1.PostService, router_deprecated_1.RouteParams, bootService_1.BootService, seoService_1.SeoService])
    ], PostPageComponent);
    return PostPageComponent;
}());
exports.PostPageComponent = PostPageComponent;
//# sourceMappingURL=postPage.component.js.map