"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var router_deprecated_1 = require('@angular/router-deprecated');
var PaginateComponent = (function () {
    function PaginateComponent(_router) {
        this._router = _router;
        this.prev = true; //czy jest prev?
        this.next = true; //czy jest next?
        this.pages = [];
        this.show = false;
    }
    PaginateComponent.prototype.ngOnChanges = function (changes) {
        if (typeof this.limit != 'undefined' &&
            typeof this.paginate != 'undefined' &&
            typeof this.page != 'undefined' &&
            typeof this.link != 'undefined') {
            this.build();
        }
    };
    PaginateComponent.prototype.goToLink = function (page) {
        var standardArg = { page: page };
        var fullArgs = Object.assign(standardArg, this.otherArgs);
        this._router.navigate([this.link, fullArgs]);
    };
    PaginateComponent.prototype.build = function () {
        //postow 200, na strone 10 = 20strony (200 / 10)
        //jestesmy na 5stronie! czyli pokazujemy 5! i 2wstecz i 2wprzod jesli sa dostepne!!
        //jestesmy na stronie 10
        var pages = Math.floor(Number(this.paginate / this.limit));
        if (pages > 1) {
            this.show = true;
            this.prev = this.page > 1 ? true : false;
            this.next = this.page < pages ? true : false;
            if (this.page - 2 > 0 && this.page - 2 <= pages)
                this.pages.push({ page: this.page - 2, selected: false });
            if (this.page - 1 > 0 && this.page - 1 <= pages)
                this.pages.push({ page: this.page - 1, selected: false });
            this.pages.push({ page: this.page, selected: true });
            if (this.page + 1 > 0 && this.page + 1 <= pages)
                this.pages.push({ page: this.page + 1, selected: false });
            if (this.page + 2 > 0 && this.page + 2 <= pages)
                this.pages.push({ page: this.page + 2, selected: false });
        }
        else {
            this.show = false;
        }
    };
    __decorate([
        core_1.Input(), 
        __metadata('design:type', Number)
    ], PaginateComponent.prototype, "limit", void 0);
    __decorate([
        //ilosc na strone
        core_1.Input(), 
        __metadata('design:type', Number)
    ], PaginateComponent.prototype, "paginate", void 0);
    __decorate([
        //ilosc postow
        core_1.Input(), 
        __metadata('design:type', Number)
    ], PaginateComponent.prototype, "page", void 0);
    __decorate([
        core_1.Input(), 
        __metadata('design:type', String)
    ], PaginateComponent.prototype, "link", void 0);
    __decorate([
        core_1.Input(), 
        __metadata('design:type', Object)
    ], PaginateComponent.prototype, "otherArgs", void 0);
    PaginateComponent = __decorate([
        core_1.Component({
            selector: 'moczniak-paginate',
            templateUrl: 'app/template/front/paginate.html'
        }), 
        __metadata('design:paramtypes', [router_deprecated_1.Router])
    ], PaginateComponent);
    return PaginateComponent;
}());
exports.PaginateComponent = PaginateComponent;
//# sourceMappingURL=paginate.component.js.map