"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var Subject_1 = require('rxjs/Subject');
var BootService = (function () {
    function BootService() {
        this._siteTitle = '';
        this._domain = 'http://specyficzny.pl/api/';
        this._token = '';
        this._isToken = new Subject_1.Subject();
        this._authLink = '?api_token=';
        this._settingsServiceURL = this._domain + 'setting';
        this._categoryServiceUrl = this._domain + 'category'; //Category
        this._postServiceURL = this._domain + 'post'; //Post
        this._pageServiceURL = this._domain + 'page'; //Page
        this._footerServiceURL = this._domain + 'footer'; //footer
        this._changeAdminPSW = this._domain + 'user';
        this._loginAdmin = this._domain + 'user/login'; //loguje PUT
        this._tryAuth = this._domain + 'tryAuth'; //sprawdza autoryzacje, GET
        this._tempStorage = {};
        this.isToken$ = this._isToken.asObservable();
    }
    BootService.prototype.getChangeAdminPsw = function () {
        return this._changeAdminPSW + this._authLink + this._token;
    };
    BootService.prototype.getTryAuth = function () {
        return this._tryAuth + this._authLink + this._token;
    };
    BootService.prototype.getLoginAdminURL = function () {
        return this._loginAdmin;
    };
    BootService.prototype.getSettingsUPDATE = function () {
        return this._settingsServiceURL + this._authLink + this._token;
    };
    BootService.prototype.getSettings = function () {
        return this._settingsServiceURL;
    };
    BootService.prototype.getFooters = function () {
        return this._footerServiceURL;
    };
    BootService.prototype.getCategoryCREATE = function () {
        return this._categoryServiceUrl + this._authLink + this._token;
    };
    BootService.prototype.getCategoryDELETE = function (categoryID) {
        return this._categoryServiceUrl + '/' + categoryID + this._authLink + this._token;
    };
    BootService.prototype.getCategories = function () {
        return this._categoryServiceUrl;
    };
    BootService.prototype.getCategoryUPDATE = function (categoryID) {
        return this._categoryServiceUrl + '/' + categoryID + this._authLink + this._token;
    };
    BootService.prototype.getPostPopular = function () {
        return this._postServiceURL + '/popular';
    };
    BootService.prototype.getPostFromCategoryPage = function (categoryID, page, limit) {
        return this._postServiceURL + '/category/' + categoryID + '/page/' + page + '?limit=' + limit;
    };
    BootService.prototype.getPostPage = function (page, limit) {
        return this._postServiceURL + '/page/' + page + '?limit=' + limit;
    };
    BootService.prototype.getPostFromCategoryCount = function (categoryID) {
        return this._postServiceURL + '/category/' + categoryID + '/count';
    };
    BootService.prototype.getPostCount = function () {
        return this._postServiceURL + '/count';
    };
    BootService.prototype.getPostUPDATE = function (postID) {
        return this._postServiceURL + '/' + postID + this._authLink + this._token;
    };
    BootService.prototype.getPostDELETE = function (postID) {
        return this._postServiceURL + '/' + postID + this._authLink + this._token;
    };
    BootService.prototype.getPostByPostID = function (postID) {
        return this._postServiceURL + '/' + postID;
    };
    BootService.prototype.getPostFromCategory = function (categoryID) {
        return this._postServiceURL + '/category/' + categoryID;
    };
    BootService.prototype.getPost = function () {
        return this._postServiceURL;
    };
    BootService.prototype.getPostCREATE = function () {
        return this._postServiceURL + this._authLink + this._token;
    };
    BootService.prototype.getPagePaginate = function (page, limit) {
        return this._pageServiceURL + '/page/' + page + '?limit=' + limit;
    };
    BootService.prototype.getPagesCount = function () {
        return this._pageServiceURL + '/count';
    };
    BootService.prototype.getPageUPDATE = function (pageID) {
        return this._pageServiceURL + '/' + pageID + this._authLink + this._token;
    };
    BootService.prototype.getPageDELETE = function (pageID) {
        return this._pageServiceURL + '/' + pageID + this._authLink + this._token;
    };
    BootService.prototype.getPageByPageID = function (pageID) {
        return this._pageServiceURL + '/' + pageID;
    };
    BootService.prototype.getPages = function () {
        return this._pageServiceURL;
    };
    BootService.prototype.getPageCREATE = function () {
        return this._pageServiceURL + this._authLink + this._token;
    };
    BootService.prototype.getSiteTitle = function () {
        return this._siteTitle;
    };
    BootService.prototype.setSiteTitle = function (title) {
        this._siteTitle = title;
    };
    BootService.prototype.setToken = function (token) {
        this._token = token;
        if (token == '')
            this._isToken.next(false);
        else
            this._isToken.next(true);
    };
    BootService.prototype.getToken = function () {
        return this._token;
    };
    BootService.prototype.getDomain = function () {
        return this._domain;
    };
    BootService.prototype.setTempKey = function (key, value) {
        this._tempStorage[key] = value;
    };
    BootService.prototype.getTempKey = function (key) {
        return this._tempStorage[key];
    };
    BootService = __decorate([
        core_1.Injectable(), 
        __metadata('design:paramtypes', [])
    ], BootService);
    return BootService;
}());
exports.BootService = BootService;
//# sourceMappingURL=bootService.js.map