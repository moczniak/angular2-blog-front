"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var http_1 = require('@angular/http');
var Rx_1 = require('rxjs/Rx');
var bootService_1 = require('./bootService');
var PostService = (function () {
    function PostService(_http, _bootService) {
        this._http = _http;
        this._bootService = _bootService;
    }
    PostService.prototype.setAllPostsSaved = function (posts) {
        this._allPosts = posts;
    };
    PostService.prototype.getAllPostsSaved = function () {
        return this._allPosts;
    };
    PostService.prototype.setPost = function (post) {
        this._post = post;
    };
    PostService.prototype.getPost = function () {
        return this._post;
    };
    PostService.prototype.getPopular = function () {
        return this._http.get(this._bootService.getPostPopular())
            .map(this.extractData)
            .catch(this.handleError);
    };
    PostService.prototype.deletePost = function (postID) {
        return this._http.delete(this._bootService.getPostDELETE(postID))
            .map(this.extractData)
            .catch(this.handleError);
    };
    PostService.prototype.getPostCount = function () {
        return this._http.get(this._bootService.getPostCount())
            .map(this.extractData)
            .catch(this.handleError);
    };
    PostService.prototype.getPostFromCategoryCount = function (categoryID) {
        return this._http.get(this._bootService.getPostFromCategoryCount(categoryID))
            .map(this.extractData)
            .catch(this.handleError);
    };
    PostService.prototype.getPostPageFromCategory = function (categoryID, page, limit) {
        if (limit === void 0) { limit = 10; }
        return this._http.get(this._bootService.getPostFromCategoryPage(categoryID, page, limit))
            .map(this.extractData)
            .catch(this.handleError);
    };
    PostService.prototype.getPostPage = function (page, limit) {
        if (limit === void 0) { limit = 10; }
        return this._http.get(this._bootService.getPostPage(page, limit))
            .map(this.extractData)
            .catch(this.handleError);
    };
    PostService.prototype.addNewPost = function (post) {
        var body = JSON.stringify({ post: post });
        return this._http.post(this._bootService.getPostCREATE(), body)
            .map(this.extractData)
            .catch(this.handleError);
    };
    PostService.prototype.updatePost = function (post) {
        var body = JSON.stringify({ post: post });
        return this._http.put(this._bootService.getPostUPDATE(post.postID), body)
            .map(this.extractData)
            .catch(this.handleError);
    };
    PostService.prototype.getPostByPostID = function (postID) {
        return this._http.get(this._bootService.getPostByPostID(postID))
            .map(this.extractData)
            .catch(this.handleError);
    };
    PostService.prototype.getAllPostsFromCategory = function (categoryID) {
        return this._http.get(this._bootService.getPostFromCategory(categoryID))
            .map(this.extractData)
            .catch(this.handleError);
    };
    PostService.prototype.getAllPosts = function () {
        return this._http.get(this._bootService.getPost())
            .map(this.extractData)
            .catch(this.handleError);
    };
    PostService.prototype.extractData = function (res) {
        if (res.status < 200 || res.status >= 300) {
            throw new Error('Response status: ' + res.status);
        }
        var body = res.json();
        return body || {};
    };
    PostService.prototype.handleError = function (error) {
        var errMsg = error || 'Server Error';
        return Rx_1.Observable.throw(errMsg);
    };
    PostService = __decorate([
        core_1.Injectable(), 
        __metadata('design:paramtypes', [http_1.Http, bootService_1.BootService])
    ], PostService);
    return PostService;
}());
exports.PostService = PostService;
//# sourceMappingURL=postService.js.map