"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var http_1 = require('@angular/http');
var Rx_1 = require('rxjs/Rx');
var bootService_1 = require('./bootService');
var PageService = (function () {
    function PageService(_http, _bootService) {
        this._http = _http;
        this._bootService = _bootService;
    }
    PageService.prototype.setAllPagesSaved = function (pages) {
        this._allPages = pages;
    };
    PageService.prototype.getAllPagesSaved = function () {
        return this._allPages;
    };
    PageService.prototype.setPage = function (page) {
        this._page = page;
    };
    PageService.prototype.getPage = function () {
        return this._page;
    };
    PageService.prototype.deletePage = function (pageID) {
        return this._http.delete(this._bootService.getPageDELETE(pageID))
            .map(this.extractData)
            .catch(this.handleError);
    };
    PageService.prototype.getPagesCount = function () {
        return this._http.get(this._bootService.getPagesCount())
            .map(this.extractData)
            .catch(this.handleError);
    };
    PageService.prototype.getPagePaginate = function (page, limit) {
        if (limit === void 0) { limit = 10; }
        return this._http.get(this._bootService.getPagePaginate(page, limit))
            .map(this.extractData)
            .catch(this.handleError);
    };
    PageService.prototype.addNewPage = function (page) {
        var body = JSON.stringify({ page: page });
        return this._http.post(this._bootService.getPageCREATE(), body)
            .map(this.extractData)
            .catch(this.handleError);
    };
    PageService.prototype.updatePost = function (page) {
        var body = JSON.stringify({ page: page });
        return this._http.put(this._bootService.getPageUPDATE(page.pageID), body)
            .map(this.extractData)
            .catch(this.handleError);
    };
    PageService.prototype.getPageByPageID = function (pageID) {
        return this._http.get(this._bootService.getPageByPageID(pageID))
            .map(this.extractData)
            .catch(this.handleError);
    };
    PageService.prototype.getAllPages = function () {
        return this._http.get(this._bootService.getPages())
            .map(this.extractData)
            .catch(this.handleError);
    };
    PageService.prototype.extractData = function (res) {
        if (res.status < 200 || res.status >= 300) {
            throw new Error('Response status: ' + res.status);
        }
        var body = res.json();
        return body || {};
    };
    PageService.prototype.handleError = function (error) {
        var errMsg = error || 'Server Error';
        return Rx_1.Observable.throw(errMsg);
    };
    PageService = __decorate([
        core_1.Injectable(), 
        __metadata('design:paramtypes', [http_1.Http, bootService_1.BootService])
    ], PageService);
    return PageService;
}());
exports.PageService = PageService;
//# sourceMappingURL=pageService.js.map