"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var http_1 = require('@angular/http');
var Rx_1 = require('rxjs/Rx');
var bootService_1 = require('./bootService');
var CategoryService = (function () {
    function CategoryService(http, _bootService) {
        this.http = http;
        this._bootService = _bootService;
    }
    CategoryService.prototype.updateCategory = function (category) {
        var body = JSON.stringify({ category: category });
        return this.http.put(this._bootService.getCategoryUPDATE(category.categoryID), body)
            .map(this.extractData)
            .catch(this.handleError);
    };
    CategoryService.prototype.addNewCategory = function (category) {
        var body = JSON.stringify({ category: category });
        return this.http.post(this._bootService.getCategoryCREATE(), body)
            .map(this.extractData)
            .catch(this.handleError);
    };
    CategoryService.prototype.deleteCategory = function (categoryID) {
        return this.http.delete(this._bootService.getCategoryDELETE(categoryID))
            .map(this.extractData)
            .catch(this.handleError);
    };
    CategoryService.prototype.getCategories = function () {
        return this.http.get(this._bootService.getCategories())
            .map(this.extractData)
            .catch(this.handleError);
    };
    CategoryService.prototype.extractData = function (res) {
        if (res.status < 200 || res.status >= 300) {
            throw new Error('Response status: ' + res.status);
        }
        var body = res.json();
        return body || {};
    };
    CategoryService.prototype.handleError = function (error) {
        var errMsg = error.message || 'Server error';
        console.error(errMsg);
        return Rx_1.Observable.throw(errMsg);
    };
    CategoryService = __decorate([
        core_1.Injectable(), 
        __metadata('design:paramtypes', [http_1.Http, bootService_1.BootService])
    ], CategoryService);
    return CategoryService;
}());
exports.CategoryService = CategoryService;
//# sourceMappingURL=categoryService.js.map