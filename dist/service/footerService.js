"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var http_1 = require('@angular/http');
var Rx_1 = require('rxjs/Rx');
var bootService_1 = require('./bootService');
//
var FooterService = (function () {
    function FooterService(_http, _bootService) {
        this._http = _http;
        this._bootService = _bootService;
    }
    FooterService.prototype.getFooters = function () {
        return this._http.get(this._bootService.getFooters())
            .map(this.extractData)
            .catch(this.handleError);
    };
    FooterService.prototype.extractData = function (res) {
        if (res.status < 200 || res.status >= 300) {
            throw new Error('Response status: ' + res.status);
        }
        var body = res.json();
        return body || {};
    };
    FooterService.prototype.handleError = function (error) {
        var errMsg = error.json() || 'Server Error';
        return Rx_1.Observable.throw(errMsg);
    };
    FooterService = __decorate([
        core_1.Injectable(), 
        __metadata('design:paramtypes', [http_1.Http, bootService_1.BootService])
    ], FooterService);
    return FooterService;
}());
exports.FooterService = FooterService;
//# sourceMappingURL=footerService.js.map