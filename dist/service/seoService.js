"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var SeoService = (function () {
    function SeoService() {
        this._defaultSiteTitle = "Blog NG2";
        this._defaultSiteDesc = "Pierwsze podejscie w ng2";
        this._defaultSiteKeywords = "angular2";
    }
    SeoService.prototype.setTitle = function (title) {
        if (title === void 0) { title = this._defaultSiteTitle; }
        document.title = title;
    };
    SeoService.prototype.setKeywords = function (keywords) {
        if (keywords === void 0) { keywords = this._defaultSiteKeywords; }
        document.querySelector('meta[name="keywords"]').setAttribute("content", keywords);
    };
    SeoService.prototype.setDescription = function (description) {
        if (description === void 0) { description = this._defaultSiteDesc; }
        document.querySelector('meta[name="description"]').setAttribute("content", description);
    };
    SeoService.prototype.setDefaultTitle = function (title) {
        this._defaultSiteTitle = title;
    };
    SeoService.prototype.setDefaultDesc = function (desc) {
        this._defaultSiteDesc = desc;
    };
    SeoService.prototype.setDefaultKeywords = function (keywords) {
        this._defaultSiteKeywords = keywords;
    };
    SeoService = __decorate([
        core_1.Injectable(), 
        __metadata('design:paramtypes', [])
    ], SeoService);
    return SeoService;
}());
exports.SeoService = SeoService;
//# sourceMappingURL=seoService.js.map