"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var LocalStorage = (function () {
    function LocalStorage() {
        this._expireMilisec = 2 * 60 * 60 * 1000;
        if (typeof (Storage) !== "undefined") {
            if (typeof localStorage['expire'] !== 'undefined') {
                var expire = parseInt(localStorage['expire']);
                var now = new Date().getTime();
                if (now >= expire) {
                    this.clearAll();
                }
            }
            else
                this.clearAll();
        }
    }
    LocalStorage.prototype.setExpiration = function () {
        if (typeof (Storage) !== "undefined") {
            var date = new Date().getTime();
            var expiration = new Date().getTime() + this._expireMilisec;
            localStorage.setItem('expire', expiration.toString());
        }
    };
    LocalStorage.prototype.setKey = function (key, value) {
        if (typeof (Storage) !== "undefined") {
            localStorage.setItem(key, value);
        }
    };
    LocalStorage.prototype.getKey = function (key) {
        return localStorage[key];
    };
    LocalStorage.prototype.removeKey = function (key) {
        localStorage.removeItem(key);
    };
    LocalStorage.prototype.clearAll = function () {
        localStorage.clear();
    };
    LocalStorage = __decorate([
        core_1.Injectable(), 
        __metadata('design:paramtypes', [])
    ], LocalStorage);
    return LocalStorage;
}());
exports.LocalStorage = LocalStorage;
//# sourceMappingURL=localStorage.js.map