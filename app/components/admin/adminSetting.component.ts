import {Component, OnInit} from '@angular/core';


import {SettingService} from './../../service/settingService';
import {UserService} from './../../service/userService';


import {Setting} from './../../class/setting';


@Component({
    selector: 'moczniak-admin-setting',
    templateUrl: 'app/template/admin/adminSetting.component.html',
    providers: [SettingService,UserService]
})


export class AdminSettingComponent implements OnInit {

    constructor(private _settingService: SettingService, private _userService : UserService) { }

    setting: Setting;

    user: any = { passwordcr: '', password: '', passwordcf: '' };

    loginErr: string = '';
    loginSucc: string = '';

    ngOnInit() {
        this.getSettings();
    }

    getSettings() {
        this._settingService.getSetting()
            .subscribe(
            settings => this.setting = settings,
            error => { }
            )
    }

    saveSettings() {
        this._settingService.saveSettings(this.setting)
            .subscribe(
            setting => console.log(setting),
            error => console.log(error)
            )
    }

    saveLoginData() {
        if (this.user.password === this.user.passwordcf) {
          this._userService.changePassword(this.user)
                .subscribe(
                  user => {
                    this.loginSucc = user.message;
                    this.hidePop();
                  },
                  error => {
                    this.loginErr = error.message;
                    this.hidePop();
                  }
                )
        }else{
          this.loginErr = 'Hasla nie zgadzają się ze sobą';
          this.hidePop();
        }
    }

    hidePop() {
        setTimeout(() => {
            this.loginErr = '';
            this.loginSucc = '';
        }, 3000);
    }


}
