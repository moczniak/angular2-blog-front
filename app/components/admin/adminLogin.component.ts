import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router-deprecated';


import {User} from './../../class/user';
import {UserService} from './../../service/userService';
import {BootService} from './../../service/bootService';
import {LocalStorage} from './../../service/localStorage';


@Component({
  selector: 'moczniak-admin-login',
  templateUrl:'app/template/admin/adminLogin.component.html',
  providers: []
})


export class AdminLoginComponent implements OnInit{

  constructor(private _userService : UserService, private _localStorage : LocalStorage, private _bootService : BootService, private _router : Router){}

  loginErr : string;

  user : User = new User('','');


  ngOnInit(){

  }

  loginNow(){
    this._userService.loginAdmin(this.user.login, this.user.password)
            .subscribe(
              user => {
                this._bootService.setToken(user.api_token);
                this._localStorage.setKey('api_token',user.api_token);
                this._localStorage.setExpiration();
                this._router.navigate(['AdminDashboard']);
              },
              error => this.loginErr = error.message
            )
  }

}
