import {Component, OnInit} from '@angular/core';
import {ROUTER_DIRECTIVES, Router, RouteParams} from '@angular/router-deprecated';
import {PostService} from './../../service/postService';

import {BootService} from './../../service/bootService';
import {LocalStorage} from './../../service/localStorage';

import {Post} from './../../class/post';

import {PaginateComponent} from './paginate.component';

@Component({
  selector: 'moczniak-admin-posts',
  templateUrl: 'app/template/admin/adminPosts.component.html',
  providers:[],
  directives:[ROUTER_DIRECTIVES, PaginateComponent]
})


export class AdminPostsComponent implements OnInit{
  constructor(private _postService : PostService, private _bootService : BootService, private _localStorage : LocalStorage, private _router : Router, private _routeParams : RouteParams){}

  posts : Post[];
  limit : number = 10;
  page : number = 1;

  pageLink : string = 'AdminPostsPage';


  paginate : number;

  ngOnInit(){
    let page = this._routeParams.get('page');
    if(page != null){
      this.page = Number(page);
    }
    this.getPosts();
    this.pagination();
  }

  deletePost(postID : number){
    this._postService.deletePost(postID)
            .subscribe(
              post=> {this.getPosts(); this.pagination()},
              error=> this.handleError(error)
            );
  }

  editPost(post : Post){
    this._postService.setPost(post);
    this._router.navigate(['AdminPostEditor',{postID: post.postID}])
  }

  getPosts(){
    this._postService.getPostPage(this.page,this.limit)
            .subscribe(
              post => this.posts = post,
              error => {}
            )
  }

  pagination(){
    this._postService.getPostCount()
          .subscribe(
            count => this.paginate = count.count,
            error => {}
          )
  }


  handleError(error : any){
    if(error.status == 401){
      this._bootService.setToken('');
      this._localStorage.clearAll();
      this._router.navigate(['AdminLogin']);
    }
  }


}
