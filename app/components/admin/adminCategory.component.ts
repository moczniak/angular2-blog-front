import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router-deprecated';

import {Category} from './../../class/category';

import {CategoryService} from './../../service/categoryService';

import {BootService} from './../../service/bootService';
import {LocalStorage} from './../../service/localStorage';



@Component({
  selector:'moczniak-admin-category',
  templateUrl: 'app/template/admin/adminCategory.component.html',
  providers:[CategoryService]
})

export class AdminCategoryComponent {

  constructor(private _categoryService : CategoryService, private _router : Router, private _bootService : BootService, private _localStorage : LocalStorage){}


  category : Category[];
  modelCategory : Category = new Category('','');
  modelCategoryEdit : Category = new Category('','');

  editCategoryStatus : boolean = false;

  tempCategory : any;

  saved : boolean = false;
  errorTxt : string = '';

  ngOnInit(){

    this.getCategories();
  }

  saveCategoryEdit(){
    this._categoryService.updateCategory(this.modelCategoryEdit)
            .subscribe(
              category=>{
                for(let i =0; i <this.category.length;++i){
                  if(this.category[i].categoryID == this.modelCategoryEdit.categoryID){
                    this.category[i] = this.modelCategoryEdit;
                        this.editCategoryStatus = false;
                        break;
                  }
                }
              },
              error=> this.handleError(error)
            )
  }



  editCategory(category : Category){
    this.modelCategoryEdit =  Object.assign({}, category);
    this.editCategoryStatus = true;
  }

  saveCategory(){
    console.log(this.modelCategory);
    this._categoryService.addNewCategory(this.modelCategory)
            .subscribe(
              category=>{
                this.category.push(category);
                this.modelCategory.title='';
                this.modelCategory.alias='';
                this.modelCategory.categoryID=0;
              },
              error=> this.handleError(error)
            )
  }

  getCategories(){
    this._categoryService.getCategories()
            .subscribe(
              category=> this.category = category,
              error=> {}
            )
  }

  deleteCategory(categoryID : number){
    this._categoryService.deleteCategory(categoryID)
          .subscribe(
            category=>{
              for(let i =0; i < this.category.length;++i){
                if(this.category[i].categoryID == categoryID){
                  this.category.splice(i, 1);
                  break;
                }
              }
            },
            error=>this.handleError(error)
          )
  }

  handleError(error : any){
    if(error.status == 401){
      this._bootService.setToken('');
      this._localStorage.clearAll();
      this._router.navigate(['AdminLogin']);
    }else{
      this.saved = true;
      this.errorTxt = 'Podany Tytuł lub alias już istnieje';
      setTimeout(()=>{
        this.saved = false;
      },3000);
    }
  }

}
