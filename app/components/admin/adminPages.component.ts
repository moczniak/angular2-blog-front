import {Component, OnInit} from '@angular/core';
import {ROUTER_DIRECTIVES, Router, RouteParams} from '@angular/router-deprecated';
import {PageService} from './../../service/pageService';

import {BootService} from './../../service/bootService';
import {LocalStorage} from './../../service/localStorage';

import {Page} from './../../class/page';

import {PaginateComponent} from './paginate.component';

@Component({
  selector: 'moczniak-admin-posts',
  templateUrl: 'app/template/admin/adminPages.component.html',
  providers:[],
  directives:[ROUTER_DIRECTIVES, PaginateComponent]
})


export class AdminPagesComponent implements OnInit{
  constructor(private _pageService : PageService, private _bootService : BootService, private _localStorage : LocalStorage, private _router : Router, private _routeParams : RouteParams){}

  pages : Page[];
  limit : number = 10;
  page : number = 1;

  pageLink : string = 'AdminPagesPaginate';


  paginate : number;

  ngOnInit(){
    let page = this._routeParams.get('page');
    if(page != null){
      this.page = Number(page);
    }
    this.getPages();
    this.pagination();
  }

  deletePage(pageID : number){
    this._pageService.deletePage(pageID)
            .subscribe(
              page=> {this.getPages(); this.pagination()},
              error=> this.handleError(error)
            );
  }

  editPage(page : Page){
    this._pageService.setPage(page);
    this._router.navigate(['AdminPageEditor',{pageID: page.pageID}])
  }

  getPages(){
    this._pageService.getPagePaginate(this.page,this.limit)
            .subscribe(
              page => this.pages = page,
              error => {}
            )
  }

  pagination(){
    this._pageService.getPagesCount()
          .subscribe(
            count => this.paginate = count.count,
            error => {}
          )
  }


  handleError(error : any){
    if(error.status == 401){
      this._bootService.setToken('');
      this._localStorage.clearAll();
      this._router.navigate(['AdminLogin']);
    }
  }


}
