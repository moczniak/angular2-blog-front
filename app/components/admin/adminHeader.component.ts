import {Component,ViewEncapsulation} from '@angular/core';
import { RouteConfig, ROUTER_DIRECTIVES, ROUTER_PROVIDERS, Router} from '@angular/router-deprecated';
import { HTTP_PROVIDERS }    from '@angular/http';

import {BootService} from './../../service/bootService';
import {UserService} from './../../service/userService';
import {LocalStorage} from './../../service/localStorage';
import {PostService} from './../../service/postService';
import {PageService} from './../../service/pageService';

import {AdminDashboardComponent} from './adminDashboard.component';
import {AdminLoginComponent} from './adminLogin.component';
import {AdminPostsComponent} from './adminPosts.component';
import {AdminPostEditorComponent} from './adminPostEditor.component';

import {AdminCategoryComponent} from './adminCategory.component';
import {AdminSettingComponent} from './adminSetting.component';
import {AdminPagesComponent} from './adminPages.component';

import {AdminPageEditorComponent} from './adminPageEditor.component';

@Component({
  selector: 'moczniak-admin',
  templateUrl: 'app/template/admin/adminHeader.component.html',
  styleUrls: ['app/css/bootstrap.min.css','app/css/stylesAdmin.css'],
  encapsulation: ViewEncapsulation.None,
  directives: [ROUTER_DIRECTIVES],
  providers:[HTTP_PROVIDERS, UserService, LocalStorage, PostService, PageService]
})


@RouteConfig([
  {
  	path: '/dashboard',
  	name: 'AdminDashboard',
  	component: AdminDashboardComponent,
  	useAsDefault:true
  },{
    path: '/settings',
    name: 'AdminSetting',
    component: AdminSettingComponent
  },{
    path: '/pages',
    name: 'AdminPages',
    component: AdminPagesComponent
  },{
    path: '/pages/page/:page',
    name: 'AdminPagesPaginate',
    component: AdminPagesComponent
  },{
    path: '/category',
    name: 'AdminCategory',
    component: AdminCategoryComponent
  },{
    path: '/login',
    name: 'AdminLogin',
    component: AdminLoginComponent
  },{
    path: '/posts',
    name: 'AdminPosts',
    component: AdminPostsComponent
  },{
    path: '/posts/page/:page',
    name: 'AdminPostsPage',
    component: AdminPostsComponent
  },{
    path: '/postEditor/:postID',
    name: 'AdminPostEditor',
    component: AdminPostEditorComponent
  },{
    path: '/pageEditor/:pageID',
    name: 'AdminPageEditor',
    component: AdminPageEditorComponent
  }
])

export class AdminHeaderComponent{

  constructor(private _userService : UserService, private _localStorage : LocalStorage, private _bootService : BootService, private _router : Router){
    this.checkLoged();

    this._bootService.isToken$.subscribe(
      value => this.loged = value
    )
  }

  loged : boolean = false;

  tryAuth(){
    this._userService.tryAuth()
          .subscribe(
            auth => this.loged = true,
            errorStatus => {
                if(errorStatus == 401){
                    this._bootService.setToken('');
                    this._localStorage.clearAll();
                    this.goToLogin();
                }
            }
          )
  }

  goToLogin(){
    this._router.navigate(['AdminLogin']);
  }

  checkLoged(){
    if(this._bootService.getToken() == ''){
      if(typeof this._localStorage.getKey('api_token') !== 'undefined'){
        this._bootService.setToken(this._localStorage.getKey('api_token'));
        this.tryAuth();
      }else this.goToLogin();
    }else this.tryAuth();
  }


}
