import {Component, OnInit} from '@angular/core';
import {CKEditor} from 'ng2-ckeditor';
import {Router, RouteParams} from '@angular/router-deprecated';

import {BootService} from './../../service/bootService';
import {PostService} from './../../service/postService';
import {LocalStorage} from './../../service/localStorage';
import {CategoryService} from './../../service/categoryService';

import {Post} from './../../class/post';
import {Category} from './../../class/category';

import {SpinnerComponent} from './../front/spinner.component';

@Component({
  selector: 'moczniak-admin-newPost',
  templateUrl: 'app/template/admin/adminPostEditor.component.html',
  directives:[CKEditor, SpinnerComponent],
  providers:[CategoryService]
})


export class AdminPostEditorComponent implements OnInit{

  constructor(private _postService : PostService, private _bootService : BootService, private _localStorage : LocalStorage, private _router : Router, private _routeParams : RouteParams, private _categoryService : CategoryService){}

  isHTTP : boolean = false;
  domain = this._bootService.getDomain();
  post = new Post();

  category : Category[];
  modelCategory : any = 0;

  saved : boolean = false;
  savedErr : boolean = false;
  errorTxt : string = '';

  ngOnInit(){
    this.loadCategory();
    let postID = parseInt(this._routeParams.get('postID'));
    if(postID != 0){
      let tmpPost = this._postService.getPost();
      if(typeof tmpPost !== "undefined"){
        this.post = tmpPost;
        if(typeof this.post.category != 'undefined') this.modelCategory = this.post.category.categoryID;
      }else{
        this.isHTTP = true;
        this._postService.getPostByPostID(postID)
                  .subscribe(
                    post => {
                      this.post = post;
                       if(this.post.promoImage != "")this.post.promoImage = this.domain+this.post.promoImage; this.isHTTP = false;
                       if(typeof this.post.category != 'undefined') this.modelCategory = this.post.category.categoryID;
                      },
                    error => {this.isHTTP = false;}
                  );
      }
    }
  }

  loadCategory(){
    this._categoryService.getCategories()
        .subscribe(
          categories =>{
            this.category = categories;
            this.category.unshift({categoryID:0, alias:'', title:"Brak"});
          },
          error => {}
        )
  }

  categoryChange($event : any){
    this.post.categoryID = $event;
  }

  postControl(){
    if(typeof this.post.postID != "undefined"){
      this.updatePost();
    }else{
      this.addNewPost();
    }
  }

  updatePost(){
    this.isHTTP = true;
    this._postService.updatePost(this.post)
          .subscribe(
            result=> this.handleSuccess(result),
            error=> this.handleError(error),
            ()=>{this.isHTTP = false; console.log("enddd");}
          )
  }

  addNewPost(){
    this.isHTTP = true;
    this._postService.addNewPost(this.post)
        .subscribe(
          result=> this.handleSuccess(result),
          error=> this.handleError(error)
        )
  }

  handleSuccess(success : any){
    this.isHTTP = false;
    this.post = success;
    if(this.post.promoImage != ""){
      this.post.promoImage = this.domain+this.post.promoImage;
    }
    this.saved = true;
    setTimeout(()=>{
      this.saved = false;
    },3000);
  }
  handleError(error : any){
    this.isHTTP = false;
    if(error.status == 401){
      this._bootService.setToken('');
      this._localStorage.clearAll();
      this._router.navigate(['AdminLogin']);
    }else{
      this.savedErr = true;
      this.errorTxt = 'Podany alias już istnieje';
      setTimeout(()=>{
        this.savedErr = false;
      },3000);
    }
  }

  filechanger($event : any){
    this.readThis($event.target);
  }

  readThis(inputValue: any){
    let file:File = inputValue.files[0];
    let myReader:FileReader = new FileReader();
    myReader.onloadend = (e)=>{
      this.post.promoImage = myReader.result;
    }
    try{
      myReader.readAsDataURL(file);
    }catch(err){
      this.post.promoImage = '';
    }
  }

}
