import {Component, OnInit} from '@angular/core';
import {CKEditor} from 'ng2-ckeditor';
import {Router, RouteParams} from '@angular/router-deprecated';

import {BootService} from './../../service/bootService';
import {PageService} from './../../service/pageService';
import {LocalStorage} from './../../service/localStorage';
import {CategoryService} from './../../service/categoryService';

import {Page} from './../../class/page';
import {Category} from './../../class/category';

import {SpinnerComponent} from './../front/spinner.component';

@Component({
  selector: 'moczniak-admin-newPost',
  templateUrl: 'app/template/admin/adminPageEditor.component.html',
  directives:[CKEditor, SpinnerComponent],
  providers:[CategoryService]
})


export class AdminPageEditorComponent implements OnInit{

  constructor(private _pageService : PageService, private _bootService : BootService, private _localStorage : LocalStorage, private _router : Router, private _routeParams : RouteParams, private _categoryService : CategoryService){}

  isHTTP : boolean = false;
  domain = this._bootService.getDomain();
  page = new Page();


  saved : boolean = false;
  savedErr : boolean = false;
  errorTxt : string = '';

  
  ngOnInit(){
    let pageID = parseInt(this._routeParams.get('pageID'));
    if(pageID != 0){
      let tmpPage = this._pageService.getPage();
      if(typeof tmpPage !== "undefined"){
        this.page = tmpPage;
      }else{
        this.isHTTP = true;
        this._pageService.getPageByPageID(pageID)
                  .subscribe(
                    page => this.page = page,
                    error => this.isHTTP = false
                  );
      }
    }
  }




  pageControl(){
    if(typeof this.page.pageID != "undefined"){
      this.updatePage();
    }else{
      this.addNewPage();
    }
  }

  updatePage(){
    this.isHTTP = true;
    this._pageService.updatePost(this.page)
          .subscribe(
            result=> this.handleSuccess(result),
            error=> this.handleError(error)
          )
  }

  addNewPage(){
    this.isHTTP = true;
    this._pageService.addNewPage(this.page)
        .subscribe(
          result=> this.handleSuccess(result),
          error=> this.handleError(error)
        )
  }

  handleSuccess(success : any){
    this.isHTTP = false;
    this.page = success;
    this.saved = true;
    setTimeout(()=>{
      this.saved = false;
    },3000);
  }
  handleError(error : any){
    this.isHTTP = false;
    if(error.status == 401){
      this._bootService.setToken('');
      this._localStorage.clearAll();
      this._router.navigate(['AdminLogin']);
    }else{
      this.savedErr = true;
      this.errorTxt = 'Podany alias już istnieje';
      setTimeout(()=>{
        this.savedErr = false;
      },3000);
    }
  }


}
