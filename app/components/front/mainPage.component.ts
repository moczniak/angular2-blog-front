import {Component, OnInit} from '@angular/core';
import { HTTP_PROVIDERS }    from '@angular/http';
import {Router, RouteParams} from '@angular/router-deprecated';


import {PaginateComponent} from './paginate.component';

import {PostService} from './../../service/postService';
import {BootService} from './../../service/bootService';
import {SeoService} from './../../service/seoService';

import {SpinnerComponent} from './spinner.component';

import {Post} from './../../class/post';

@Component({
  selector:'moczniak-posts',
  templateUrl: 'app/template/front/mainPage.component.html',
  directives:[PaginateComponent,SpinnerComponent],
  providers:[HTTP_PROVIDERS]
})


export class MainPageComponent implements OnInit{

  constructor(private _bootService : BootService, private _postService : PostService, private _router: Router, private _routeParams : RouteParams, private _seoService : SeoService){}

  domain : string = this._bootService.getDomain();
  posts : Post[];
  errMessage : string;

  limit : number = 2;
  page : number = 1;

  pageLink : string = 'StartPages';
  isHTTP : boolean = false;

  paginate : number;

  otherArgsLink : any = {};


  ngOnInit(){
    let categoryID = this._routeParams.get('categoryID');
    if(categoryID != null){
      this.mainPageFromCategory();
    }else{
      this.mainPage();
    }

    this._seoService.setTitle();
    this._seoService.setDescription();
    this._seoService.setKeywords();

  }

  mainPageFromCategory(){
    let categoryID = Number(this._routeParams.get('categoryID'));
    let alias = this._routeParams.get('alias');
    let page = this._routeParams.get('page');

    this.pageLink = 'CategoryPages';
    this.otherArgsLink = {categoryID: categoryID, alias: alias};
    if(page != null){
      this.page = Number(page);
    }
    this.getPostsFromCategory(categoryID);
    this.paginationFromCategory(categoryID);
  }

  mainPage(){
    let page = this._routeParams.get('page');
    let tempPaginate = this._bootService.getTempKey('paginate');
    if(page != null){
      this.page = Number(page);
    }
    this.getPosts();
    if(typeof tempPaginate == 'undefined'){
      this.pagination();
    }else{
      this.paginate = tempPaginate;
    }
  }

  routePost(post : Post){
    this._postService.setPost(post);
    this._router.navigate(['Post',{postID: post.postID, alias: post.alias}])
  }


  getPostsFromCategory(categoryID : number){
    this.isHTTP = true;
    this._postService.getPostPageFromCategory(categoryID, this.page, this.limit)
          .subscribe(
            post => {this.posts = post, this.isHTTP = false;},
            error => { this.isHTTP = false;}
          )
  }

  getPosts(){
    this.isHTTP = true;
    this._postService.getPostPage(this.page,this.limit)
            .subscribe(
              post => {this.posts = post, this.isHTTP = false;},
              error => {this.isHTTP = false;}
            )
  }

  paginationFromCategory(categoryID : number){
    this.isHTTP = true;
    this._postService.getPostFromCategoryCount(categoryID)
          .subscribe(
            count => {this.paginate = count.count;this.isHTTP = false;},
            error => {this.isHTTP = false;}
          )
  }


  pagination(){
    this._postService.getPostCount()
          .subscribe(
            count => {this.paginate = count.count; this._bootService.setTempKey('paginate',count.count)},
            error => {}
          )
  }


}
