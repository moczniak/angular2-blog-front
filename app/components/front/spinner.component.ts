import {Component, Input, OnInit, OnChanges,SimpleChange } from '@angular/core';

@Component({
    selector: 'moczniak-spinner',
    templateUrl: 'app/template/front/spinner.html'
})
export class SpinnerComponent implements OnChanges, OnInit {

  constructor(){ }

  @Input()
  loading : boolean = false;

  private _timeout :any;
  show : boolean = false;

  ngOnChanges(changes: {[loading: string]: SimpleChange}) {
    changes['loading'].currentValue == true ? this.startShowing() : this.stopShowing();
  }



  startShowing(){
    this._timeout = setTimeout(()=>{
      this.show = true;
    },500);
  }

  stopShowing(){
    clearTimeout(this._timeout);
    this.show = false;
  }


  ngOnInit(){
    document.getElementById('overlay').style.height = document.body.scrollHeight+'px';
  }
}
