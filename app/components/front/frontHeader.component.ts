import {Component,ViewEncapsulation, OnInit} from '@angular/core';
import { RouteConfig, ROUTER_DIRECTIVES, ROUTER_PROVIDERS, Router} from '@angular/router-deprecated';
import { HTTP_PROVIDERS }    from '@angular/http';

import {MainPageComponent} from './mainPage.component';
import {PostPageComponent} from './postPage.component';

import {PostService} from './../../service/postService';
import {CategoryService} from './../../service/categoryService';
import {SettingService} from './../../service/settingService';
import {SeoService} from './../../service/seoService';
import {BootService} from './../../service/bootService';

import {SidebarComponent} from './sidebar.component';
import {FooterComponent} from './footer.component';

import {Category} from './../../class/category';

@Component({
  selector: 'moczniak-front',
  templateUrl: 'app/template/front/frontHeader.component.html',
  styleUrls: ['app/css/bootstrapCyborg.min.css','app/css/styles.css'],
  encapsulation: ViewEncapsulation.None,
  directives: [ROUTER_DIRECTIVES,SidebarComponent,FooterComponent],
  providers:[PostService,CategoryService, HTTP_PROVIDERS, SeoService, SettingService]
})

@RouteConfig([
  {
  	path: '/',
  	name: 'Start',
  	component: MainPageComponent,
  	useAsDefault:true
  },{
    path: '/page/:page',
    name: 'StartPages',
    component: MainPageComponent
  },{
    path: '/post/:postID/:alias',
    name: 'Post',
    component: PostPageComponent
  },{
    path: '/category/:categoryID/:alias',
    name: 'Category',
    component: MainPageComponent,
  },{
    path: '/category/:categoryID/:alias/page/:page',
    name: 'CategoryPages',
    component: MainPageComponent
  }
])


export class FrontHeaderComponent {

  constructor(private _router : Router, private _categoryService : CategoryService, private _settingService : SettingService, private _seoService : SeoService, private _bootService : BootService){

    this._router.subscribe(
      val=>console.log(val)
    )

  }
  title = '';

  categories : Category[];
  errMessage : string;

  categoryActive : any = 0;


  ngOnInit(){
    this.loadCategories();
    this.loadSettings();
    $("body").tooltip({
        selector: '[data-toggle="tooltip"]'
    });
    console.log("load!");
  }

  loadCategories(){
    this._categoryService.getCategories()
            .subscribe(
              categories => this.categories = categories,
              error => this.errMessage = error
            )
  }

  loadSettings(){
    this._settingService.getSetting()
          .subscribe(
            setting => {
              this._bootService.setSiteTitle(setting.siteTitle);

              this._seoService.setTitle(setting.title);
              this._seoService.setKeywords(setting.keywords);
              this._seoService.setDescription(setting.description);

              this._seoService.setDefaultDesc(setting.description);
              this._seoService.setDefaultTitle(setting.title);
              this._seoService.setDefaultKeywords(setting.keywords);

              this.title = setting.siteTitle;
            },
            error => console.log(error)
          )
  }

  isActiveRoute(routeName: string) {
    return routeName == this._router.currentInstruction.component.routeName;
  }


}
