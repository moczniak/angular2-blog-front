import {Component, OnInit} from '@angular/core';
import {RouteParams} from '@angular/router-deprecated';

import {PostService} from './../../service/postService';
import {BootService} from './../../service/bootService';
import {SeoService} from './../../service/seoService';

import {SpinnerComponent} from './spinner.component';

import {Post} from './../../class/post';

@Component({
  selector: 'moczniak-post',
  templateUrl: 'app/template/front/postPage.component.html',
  directives:[SpinnerComponent]
})

export class PostPageComponent implements OnInit{
  constructor(private _postService : PostService, private _routeParams : RouteParams, private _bootService : BootService, private _seoService : SeoService){}

domain : string = this._bootService.getDomain();
isHTTP : boolean = false;
post : Post;
errorMessage : string;

  ngOnInit(){
    this.getPost();



  }



  getPost(){
		let postID = this._routeParams.get('postID');
		let post = this._postService.getPost();
		if(typeof post === 'undefined'){
      this.loadPost(parseInt(postID));
		}else{
        if(post.postID == parseInt(postID)){
          this.post = post;
          this.setSeo();
          this.setComment(post.postID);
        }else{
          this.loadPost(parseInt(postID));
        }
    }
	}

  setComment(postID : number){

    DISQUS.reset({
      reload: true,
      config: function () {
        this.page.identifier = postID;
      }
    });

  }

  loadPost(postID : number){
    this.isHTTP = true;
    this._postService.getPostByPostID(postID)
      .subscribe(
        post => {
          this.post = post;
          this.setSeo();
          this.setComment(post.postID);
        },
        error => this.errorMessage = error,
        () => this.isHTTP = false
      )
  }

  setSeo(){
    this._seoService.setTitle(this.post.seoTitle);
    this._seoService.setDescription(this.post.seoDesc);
    this._seoService.setKeywords(this.post.seoKeywords);
  }



}
