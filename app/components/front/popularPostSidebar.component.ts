import {Component, OnInit} from '@angular/core';
import {ROUTER_DIRECTIVES} from '@angular/router-deprecated';

import {BootService} from './../../service/bootService';
import {PostService} from './../../service/postService';

import {Post} from './../../class/post';

@Component({
  selector: 'moczniak-sidebar-popularPost',
  templateUrl: 'app/template/front/popularPostSidebar.component.html',
  directives:[ROUTER_DIRECTIVES]
})


export class PopularPostSidebarComponent implements OnInit{

  constructor(private _postService : PostService, private _bootService : BootService){}

  popular : Post[];

  domain : string = this._bootService.getDomain();

  ngOnInit(){
    this.GetPopularPost();
  }

  GetPopularPost(){
    this._postService.getPopular()
          .subscribe(
            popular=>this.popular = popular,
            error=>{}
          )
  }

}
