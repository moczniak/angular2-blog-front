import {Component} from '@angular/core';


import {PopularPostSidebarComponent} from './popularPostSidebar.component';
import {CustomSidebarComponent} from './customSidebar.component';


@Component({
	selector: 'moczniak-sidebar',
	templateUrl: 'app/template/front/sidebar.component.html',
	directives:[PopularPostSidebarComponent, CustomSidebarComponent]

})


export class SidebarComponent{

}
