import { Component, OnInit } from '@angular/core';
import {ROUTER_DIRECTIVES, Router} from '@angular/router-deprecated';

import {FooterService} from './../../service/footerService';

import {PageService} from './../../service/pageService';

import {BootService} from './../../service/bootService';
import {PostService} from './../../service/postService';

import {Post} from './../../class/post';


@Component({
  selector: 'moczniak-footer',
  templateUrl: 'app/template/front/footer.component.html',
  providers:[FooterService,PageService],
  directives:[ROUTER_DIRECTIVES]
})



export class FooterComponent implements OnInit {

  constructor(private _router : Router,private _footerService : FooterService, private _pageService : PageService,private _postService : PostService, private _bootService : BootService){}

  content : any = [];

  popular : Post[];

  ngOnInit(){
    this.getFooters();
  }


  getFooters(){
    this._footerService.getFooters()
          .subscribe(
            footer=>{
              for(let i =0; i < footer.length; ++i){
                try{
                  let data = JSON.parse(footer[i].content);
                  if(data[0] == 'page'){
                    this.getPage(data[1].pageID,footer[i].position);
                  }else if(data[0] == 'popular'){
                    this.getPopular(footer[i].position);
                  }
                }catch(err){
                  this.content.push({content:footer[i].content, position: footer[i].position});
                }
              }
            },
            error=>{console.log(error)}
          )
  }

  getPopular(position: number){
    this._postService.getPopular()
          .subscribe(
            popular=>{
              this.popular = popular;
              this.content.push({content:'popular', position: position});
            },
            error=>{}
          )
  }

  routePost(post : Post){
    this._postService.setPost(post);
    this._router.navigate(['Post',{postID: post.postID, alias: post.alias}])
  }

  getPage(pageID : number,position: number){
    this._pageService.getPageByPageID(pageID)
          .subscribe(
            page => {
              this.content.push({content: page.content, position: position});
            },
            error =>{ console.log(error)}
          )
  }

}
