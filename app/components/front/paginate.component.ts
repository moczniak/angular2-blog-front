import {Component, OnChanges, Input} from '@angular/core';
import {Router} from '@angular/router-deprecated';



@Component({
  selector: 'moczniak-paginate',
  templateUrl: 'app/template/front/paginate.html'
})


export class PaginateComponent implements OnChanges{

  constructor(private _router : Router){

  }

  @Input()
  limit : number; //ilosc na strone

  @Input()
  paginate : number; //ilosc postow

  @Input()
  page : number;

  @Input()
  link : string;

  @Input()
  otherArgs : any;

  prev : boolean = true; //czy jest prev?
  next : boolean = true; //czy jest next?
  pages : any = [];

  show : boolean = false;

  ngOnChanges(changes : any){
    if(typeof this.limit != 'undefined' &&
      typeof this.paginate != 'undefined' &&
      typeof this.page != 'undefined' &&
      typeof this.link != 'undefined'){
        this.build();
      }
  }

  goToLink(page : number){
    let standardArg = {page : page};
    let fullArgs = Object.assign(standardArg, this.otherArgs);
    this._router.navigate([this.link,fullArgs])
  }

  build(){
    //postow 200, na strone 10 = 20strony (200 / 10)
    //jestesmy na 5stronie! czyli pokazujemy 5! i 2wstecz i 2wprzod jesli sa dostepne!!

    //jestesmy na stronie 10
    let pages  = Math.floor(Number(this.paginate / this.limit));
    if(pages > 1){
      this.show = true;
      this.prev = this.page > 1 ? true : false;
      this.next = this.page < pages ? true : false;

      if(this.page-2 > 0 && this.page-2 <= pages)  this.pages.push({page : this.page-2, selected : false});
      if(this.page-1 > 0 && this.page-1 <= pages)  this.pages.push({page : this.page-1, selected : false});
        this.pages.push({page : this.page, selected : true});
      if(this.page+1 > 0 && this.page+1 <= pages)  this.pages.push({page : this.page+1, selected : false});
      if(this.page+2 > 0 && this.page+2 <= pages)  this.pages.push({page : this.page+2, selected : false});
    }else{
      this.show = false;
    }


  }



}
