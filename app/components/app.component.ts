import { Component,OnInit } from '@angular/core';
import { RouteConfig, ROUTER_DIRECTIVES, ROUTER_PROVIDERS, Router} from '@angular/router-deprecated';

import {FrontHeaderComponent} from './front/frontHeader.component';
import {AdminHeaderComponent} from './admin/adminHeader.component';

import {BootService} from './../service/bootService';
import {SeoService} from './../service/seoService';

@Component({
  selector: 'moczniak-app',
  template: `

      <router-outlet></router-outlet>

  `,
  directives: [ROUTER_DIRECTIVES],
  providers: [ROUTER_PROVIDERS, BootService, SeoService]
})


@RouteConfig([
  {
	path: '/...',
	name: 'App',
	component: FrontHeaderComponent,
	useAsDefault:true
  },{
    path: '/admin/...',
    name: 'Admin',
    component: AdminHeaderComponent
  }
])


export class AppComponent {



}
