import {Injectable} from '@angular/core';

import {Http, Response,Headers, RequestOptions} from '@angular/http';
import {Observable} from 'rxjs/Rx';
import {BootService} from './bootService';
import {Post} from '../class/post';

@Injectable()
export class PostService{

	constructor(private _http:Http, private _bootService:BootService){}

	private _post: Post;
	private _allPosts : Post[];

	setAllPostsSaved(posts: Post[]){
		this._allPosts = posts;
	}

	getAllPostsSaved(){
		return this._allPosts;
	}

	setPost(post: Post){
		this._post = post;
	}

	getPost()
	{
		return this._post;
	}


	getPopular(): Observable<Post[]>{
		return this._http.get(this._bootService.getPostPopular())
				.map(this.extractData)
				.catch(this.handleError);
	}

	deletePost(postID : number){
		return this._http.delete(this._bootService.getPostDELETE(postID))
				.map(this.extractData)
				.catch(this.handleError);
	}
	getPostCount(){
		return this._http.get(this._bootService.getPostCount())
				.map(this.extractData)
				.catch(this.handleError);
	}

	getPostFromCategoryCount(categoryID: number){
		return this._http.get(this._bootService.getPostFromCategoryCount(categoryID))
				.map(this.extractData)
				.catch(this.handleError);
	}

	getPostPageFromCategory(categoryID : number, page : number, limit : number = 10): Observable<Post[]>{
		return this._http.get(this._bootService.getPostFromCategoryPage(categoryID, page,limit))
					.map(this.extractData)
					.catch(this.handleError);
	}

	getPostPage(page : number, limit : number = 10): Observable<Post[]>{
		return this._http.get(this._bootService.getPostPage(page,limit))
					.map(this.extractData)
					.catch(this.handleError);
	}

	addNewPost(post: Post){
		let body = JSON.stringify({ post });
  	return this._http.post(this._bootService.getPostCREATE(), body)
                    .map(this.extractData)
                    .catch(this.handleError);
	}

	updatePost(post : Post){
		let body = JSON.stringify({post});
		return this._http.put(this._bootService.getPostUPDATE(post.postID),body)
				.map(this.extractData)
				.catch(this.handleError);
	}

	getPostByPostID(postID : number): Observable<Post>{
		return this._http.get(this._bootService.getPostByPostID(postID))
			.map(this.extractData)
			.catch(this.handleError);
	}


	getAllPostsFromCategory(categoryID : number): Observable<Post[]>{
		return this._http.get(this._bootService.getPostFromCategory(categoryID))
			.map(this.extractData)
			.catch(this.handleError);
	}

	getAllPosts():Observable<Post[]>{
		return this._http.get(this._bootService.getPost())
			.map(this.extractData)
			.catch(this.handleError);
	}

	private extractData(res : Response){
		if(res.status < 200 || res.status >= 300){
			throw new Error('Response status: '+res.status);
		}
		let body = res.json();
		return body || {};
	}

	private handleError(error: any){
		let errMsg = error || 'Server Error';
		return Observable.throw(errMsg);
	}

}
