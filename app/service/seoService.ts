import {Injectable} from '@angular/core';

@Injectable()
export class SeoService{

  private _defaultSiteTitle :string = "Blog NG2";
	private _defaultSiteDesc :string = "Pierwsze podejscie w ng2";
	private _defaultSiteKeywords :string = "angular2";


  setTitle(title : string = this._defaultSiteTitle){
    document.title = title;
  }

  setKeywords(keywords : string = this._defaultSiteKeywords){
    document.querySelector('meta[name="keywords"]').setAttribute("content", keywords);
  }

  setDescription(description : string = this._defaultSiteDesc){
    document.querySelector('meta[name="description"]').setAttribute("content", description);
  }



  setDefaultTitle(title : string){
    this._defaultSiteTitle = title;
  }

  setDefaultDesc(desc : string){
    this._defaultSiteDesc = desc;
  }

  setDefaultKeywords(keywords : string){
    this._defaultSiteKeywords = keywords;
  }

}
