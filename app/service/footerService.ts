import {Injectable} from '@angular/core';
import {Http, Response,Headers, RequestOptions} from '@angular/http';
import {Observable} from 'rxjs/Rx';
import {BootService} from './bootService';

import {Footer} from '../class/footer';
//
@Injectable()
export class FooterService{

constructor(private _http: Http, private _bootService:BootService){}

  getFooters():Observable<Footer[]>{
    return this._http.get(this._bootService.getFooters())
        .map(this.extractData)
        .catch(this.handleError);
  }


  private extractData(res : Response){
		if(res.status < 200 || res.status >= 300){
			throw new Error('Response status: '+res.status);
		}
		let body = res.json();
		return body || {};

	}

	private handleError(error: any){
		let errMsg = error.json() || 'Server Error';
		return Observable.throw(errMsg);
	}

}
