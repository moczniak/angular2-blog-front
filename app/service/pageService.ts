import {Injectable} from '@angular/core';

import {Http, Response,Headers, RequestOptions} from '@angular/http';
import {Observable} from 'rxjs/Rx';
import {BootService} from './bootService';
import {Page} from '../class/page';

@Injectable()
export class PageService{

	constructor(private _http:Http, private _bootService:BootService){}

	private _page: Page;
	private _allPages : Page[];

	setAllPagesSaved(pages: Page[]){
		this._allPages = pages;
	}

	getAllPagesSaved(){
		return this._allPages;
	}

	setPage(page: Page){
		this._page = page;
	}

	getPage()
	{
		return this._page;
	}

	deletePage(pageID : number){
		return this._http.delete(this._bootService.getPageDELETE(pageID))
				.map(this.extractData)
				.catch(this.handleError);
	}
	getPagesCount(){
		return this._http.get(this._bootService.getPagesCount())
				.map(this.extractData)
				.catch(this.handleError);
	}


	getPagePaginate(page : number, limit : number = 10): Observable<Page[]>{
		return this._http.get(this._bootService.getPagePaginate(page,limit))
					.map(this.extractData)
					.catch(this.handleError);
	}

	addNewPage(page: Page){
		let body = JSON.stringify({ page });
  	return this._http.post(this._bootService.getPageCREATE(), body)
                    .map(this.extractData)
                    .catch(this.handleError);
	}

	updatePost(page : Page){
		let body = JSON.stringify({page});
		return this._http.put(this._bootService.getPageUPDATE(page.pageID),body)
				.map(this.extractData)
				.catch(this.handleError);
	}

	getPageByPageID(pageID : number): Observable<Page>{
		return this._http.get(this._bootService.getPageByPageID(pageID))
			.map(this.extractData)
			.catch(this.handleError);
	}

	getAllPages():Observable<Page[]>{
		return this._http.get(this._bootService.getPages())
			.map(this.extractData)
			.catch(this.handleError);
	}

	private extractData(res : Response){
		if(res.status < 200 || res.status >= 300){
			throw new Error('Response status: '+res.status);
		}
		let body = res.json();
		return body || {};
	}

	private handleError(error: any){
		let errMsg = error || 'Server Error';
		return Observable.throw(errMsg);
	}

}
