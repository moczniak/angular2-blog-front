import {Injectable} from '@angular/core';
import { Http, Response } from '@angular/http';

import {Observable} from 'rxjs/Rx';
import {Category} from '../class/category';
import {BootService} from './bootService';


@Injectable()
export class CategoryService{

	constructor (private http: Http, private _bootService:BootService){}



	updateCategory(category : Category): Observable<Category>{
		let body = JSON.stringify({category});
		return this.http.put(this._bootService.getCategoryUPDATE(category.categoryID),body)
				.map(this.extractData)
				.catch(this.handleError);
	}

	addNewCategory(category : Category): Observable<Category>{
		let body = JSON.stringify({category});
		return this.http.post(this._bootService.getCategoryCREATE(),body)
				.map(this.extractData)
				.catch(this.handleError);
	}

	deleteCategory(categoryID : number){
		return this.http.delete(this._bootService.getCategoryDELETE(categoryID))
				.map(this.extractData)
				.catch(this.handleError);
	}


	getCategories (): Observable<Category[]> {
		return this.http.get(this._bootService.getCategories())
				.map(this.extractData)
				.catch(this.handleError);
	}

	private extractData(res: Response){
		if(res.status < 200 || res.status >= 300){
			throw new Error('Response status: '+res.status);
		}
		let body = res.json();
		return body || {};
	}
	private handleError(error: any){
		let errMsg = error.message || 'Server error';
		console.error(errMsg);
		return Observable.throw(errMsg);
	}
}
