import {Injectable} from '@angular/core';
import {Http, Response,Headers, RequestOptions} from '@angular/http';
import {Observable} from 'rxjs/Rx';
import {BootService} from './bootService';

import {User} from '../class/user';

@Injectable()
export class UserService{

constructor(private _http: Http, private _bootService:BootService){}


  changePassword(passwords : any){
    let body = JSON.stringify({passwords});
    return this._http.put(this._bootService.getChangeAdminPsw(), body)
        .map(this.extractData)
        .catch(this.handleError);
  }

  tryAuth(){
    return this._http.get(this._bootService.getTryAuth())
                    .map(this.extractData)
                    .catch(this.errorCode);
  }

  loginAdmin (login: string, password: string): Observable<User> {
    let body = JSON.stringify({ login,password });

    return this._http.put(this._bootService.getLoginAdminURL(), body)
                    .map(this.extractData)
                    .catch(this.handleError);
  }



  private extractData(res : Response){
		if(res.status < 200 || res.status >= 300){
			throw new Error('Response status: '+res.status);
		}
		let body = res.json();
		return body || {};

	}

	private handleError(error: any){
		let errMsg = error.json() || 'Server Error';
		return Observable.throw(errMsg);
	}

  private errorCode(error : any){
		return Observable.throw(error.status);
  }


}
