import {Injectable} from '@angular/core';
import {Http, Response,Headers, RequestOptions} from '@angular/http';
import {Observable} from 'rxjs/Rx';

import {Setting} from './../class/setting';

import {BootService} from './bootService';

@Injectable()
export class SettingService{

	constructor(private _http:Http, private _bootService:BootService){}


	saveSettings(setting : Setting):Observable<Setting>{
		let body = JSON.stringify({setting});
		return this._http.put(this._bootService.getSettingsUPDATE(), body)
				.map(this.extractData)
				.catch(this.handleError);
	}

	getSetting (): Observable<Setting> {
    return this._http.get(this._bootService.getSettings())
        .map(this.extractData)
        .catch(this.handleError);
  }

  private extractData(res : Response){
		if(res.status < 200 || res.status >= 300){
			throw new Error('Response status: '+res.status);
		}
		let body = res.json();
		return body || {};
	}

	private handleError(error: any){
		let errMsg = error || 'Server Error';
		return Observable.throw(errMsg);
	}



}
