import {Injectable} from '@angular/core';
import { Subject }    from 'rxjs/Subject';

@Injectable()
export class BootService{

	private _siteTitle = '';
	private _domain = 'http://specyficzny.pl/api/';
	private _token = '';
	private _isToken = new Subject<boolean>();


	private _authLink = '?api_token=';
	private _settingsServiceURL = this._domain+'setting';
	private _categoryServiceUrl = this._domain+'category';//Category
	private _postServiceURL = this._domain+'post';//Post
	private _pageServiceURL = this._domain+'page';//Page
	private _footerServiceURL = this._domain+'footer';//footer

	private _changeAdminPSW = this._domain+'user';
	private _loginAdmin = this._domain+'user/login';//loguje PUT
	private _tryAuth = this._domain+'tryAuth';//sprawdza autoryzacje, GET

	private _tempStorage = {};


  isToken$ = this._isToken.asObservable();

	constructor(){}


	getChangeAdminPsw(){
		return this._changeAdminPSW+this._authLink+this._token;
	}

	getTryAuth(){
		return this._tryAuth+this._authLink+this._token;
	}

	getLoginAdminURL(){
		return this._loginAdmin;
	}

	getSettingsUPDATE(){
		return this._settingsServiceURL+this._authLink+this._token;
	}

	getSettings(){
		return this._settingsServiceURL;
	}

	getFooters(){
		return this._footerServiceURL;
	}

	getCategoryCREATE(){
		return this._categoryServiceUrl+this._authLink+this._token;
	}

	getCategoryDELETE(categoryID : number){
		return this._categoryServiceUrl+'/'+categoryID+this._authLink+this._token;
	}

	getCategories(){
		return this._categoryServiceUrl;
	}

	getCategoryUPDATE(categoryID : number){
		return this._categoryServiceUrl+'/'+categoryID+this._authLink+this._token;
	}





	getPostPopular(){
		return this._postServiceURL+'/popular';
	}

	getPostFromCategoryPage(categoryID : number, page : number, limit: number){
		return this._postServiceURL+'/category/'+categoryID+'/page/'+page+'?limit='+limit;
	}

	getPostPage(page : number, limit : number){
		return this._postServiceURL+'/page/'+page+'?limit='+limit;
	}

	getPostFromCategoryCount(categoryID : number){
		return this._postServiceURL+'/category/'+categoryID+'/count';
	}

	getPostCount(){
		return this._postServiceURL+'/count';
	}

	getPostUPDATE(postID : number){
		return this._postServiceURL+'/'+postID+this._authLink+this._token;
	}

	getPostDELETE(postID : number){
		return this._postServiceURL+'/'+postID+this._authLink+this._token;
	}

	getPostByPostID(postID : number){
		return this._postServiceURL+'/'+postID;
	}

	getPostFromCategory(categoryID : number){
		return this._postServiceURL+'/category/'+categoryID;
	}

	getPost(){
		return this._postServiceURL;
	}

	getPostCREATE(){
		return this._postServiceURL+this._authLink+this._token;
	}




	getPagePaginate(page : number, limit : number){
		return this._pageServiceURL+'/page/'+page+'?limit='+limit;
	}


	getPagesCount(){
		return this._pageServiceURL+'/count';
	}

	getPageUPDATE(pageID : number){
		return this._pageServiceURL+'/'+pageID+this._authLink+this._token;
	}

	getPageDELETE(pageID : number){
		return this._pageServiceURL+'/'+pageID+this._authLink+this._token;
	}

	getPageByPageID(pageID : number){
		return this._pageServiceURL+'/'+pageID;
	}


	getPages(){
		return this._pageServiceURL;
	}

	getPageCREATE(){
		return this._pageServiceURL+this._authLink+this._token;
	}






	getSiteTitle(){
		return this._siteTitle;
	}

	setSiteTitle(title : string){
		this._siteTitle = title;
	}

	setToken(token: string){
		this._token = token;
		if(token == '')
			this._isToken.next(false);
		else
		this._isToken.next(true);
	}

	getToken(){
		return this._token;
	}

	getDomain(){
		return this._domain;
	}

	setTempKey(key : string, value : any){
		this._tempStorage[key] = value;
	}

	getTempKey(key : string){
		return this._tempStorage[key];
	}

}
