import {Injectable} from '@angular/core';

@Injectable()
export class LocalStorage{

  private _expireMilisec  =  2*60*60*1000;

  constructor(){
    if(typeof(Storage) !== "undefined") {
      if(typeof localStorage['expire'] !== 'undefined'){
        let expire = parseInt(localStorage['expire']);
        let now = new Date().getTime();
        if(now >= expire){
          this.clearAll();
        }
      }else this.clearAll();
    }
  }

  setExpiration(){
    if(typeof(Storage) !== "undefined") {
        let date = new Date().getTime();
        let expiration = new Date().getTime() + this._expireMilisec;
        localStorage.setItem('expire', expiration.toString());
    }
  }

  setKey(key:string, value: string){
    if(typeof(Storage) !== "undefined") {
        localStorage.setItem(key, value);
    }
  }

  getKey(key:string){
    return  localStorage[key];
  }

  removeKey(key:string){
    localStorage.removeItem(key);
  }

  clearAll(){
    localStorage.clear();
  }

}
