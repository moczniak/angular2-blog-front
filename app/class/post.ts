import {Category} from './category';

export class Post{
  postID:number;
  title: string;
  alias:string;
  categoryID: number;
  category: Category;
  content: string;
  promoImage: string;
  publishDate: string;
  publishDateStamp : number;
  published:boolean;
  visited: number;
  seoTitle : string;
  seoDesc : string;
  seoKeywords : string;
}
