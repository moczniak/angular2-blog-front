export class Page{
  pageID:number;
  title: string;
  alias:string;
  content: string;
  publishDate: string;
  publishDateStamp : number;
  published:boolean;
  visited: number;
  seoTitle : string;
  seoDesc : string;
  seoKeywords : string;
}
