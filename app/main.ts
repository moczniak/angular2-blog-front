import { bootstrap }    from '@angular/platform-browser-dynamic';

import { AppComponent } from './components/app.component';

import {BootService} from './service/bootService';
import {LocalStorage} from './service/localStorage';

bootstrap(AppComponent, [BootService, LocalStorage]);
